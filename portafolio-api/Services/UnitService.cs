using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using portafolio_api.Models.Dto.Unit;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;

namespace portafolio_api.Services
{
    public class UnitService
    {
        private readonly ILogger<UnitService> _logger;

        private readonly UnitRepository _unitRepository;

        
        public UnitService(ILogger<UnitService> logger, UnitRepository unitRepository)
        {
            _logger = logger;
            _unitRepository = unitRepository;
        }

        public async Task<GlobalResponse<List<GetUnitDto>>> GetAllUnit()
        {
            GlobalResponse<List<GetUnitDto>> response = new GlobalResponse<List<GetUnitDto>>();
            try
            {
                var units = await _unitRepository.getAll();

                response.code = "000";
                response.message = "Unidades Obtenidas";
                response.data = units;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAllUnit] Error al obtener todas las unidades", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<GetUnitDto>> GetId(int idUnit)
        {
            GlobalResponse<GetUnitDto> response = new GlobalResponse<GetUnitDto>();
            try
            {
                var foundUnit = await _unitRepository.getUnitId(idUnit);

                response.code = "000";
                response.message = "Unidad Obtenida";
                response.data = foundUnit;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetId] Error al obtener la unidad", ex.Message);
                throw;
            }
        }
    
        public async Task<GlobalResponse<object>> GetReportUnits(string initDate, string lastDate)
        {
            GlobalResponse<object> response = new GlobalResponse<object>();
            try
            {
                var anexos = await _unitRepository.getUnitReport(initDate, lastDate, 0 , false);

                response.code = "000";
                response.message = "Reporte Unidades Obtenido";
                response.data = anexos;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetReportUnits] Error al obtener el reporte de unidades: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<string>> Create(CreateUpdateUnitDto createUpdateUnitDto)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                string codeResp = await _unitRepository.create(createUpdateUnitDto);

                if (codeResp.Equals("001"))
                {
                    _logger.LogInformation("[Create] Ya existe Unidad con nombre ingresado");
                    throw new CustomErrorsExceptions("Ya existe Unidad con nombre ingresado", 200, "001");
                }

                response.code = "000";
                response.message = "Unidad Creada";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Create] Error al crear unidad: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<string>> Update(CreateUpdateUnitDto createUpdateUnitDto, int idUnit)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                string codeResp = await _unitRepository.update(createUpdateUnitDto, idUnit);

                if (codeResp.Equals("001"))
                {
                    _logger.LogInformation("[Update] Unidad no existe");
                    throw new CustomErrorsExceptions("Unidad no existe", 200, "001");
                } else if (codeResp.Equals("002"))
                {
                    _logger.LogInformation("[Update] Existe unidad con ese nombre");
                    throw new CustomErrorsExceptions("Existe unidad con ese nombre", 200, "002");
                }

                response.code = "000";
                response.message = "Unidad Actualizada";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Update] Error al actualizar unidad: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<GetUnitDto>>> Delete(int idUnit)
        {
            GlobalResponse<List<GetUnitDto>> response = new GlobalResponse<List<GetUnitDto>>();
            try
            {
                string codeResp = await _unitRepository.delete(idUnit);

                if (codeResp.Equals("001"))
                {
                    _logger.LogInformation("[Delete] Unidad no existe");
                    throw new CustomErrorsExceptions("Unidad no existe", 200, "001");
                }

                var units = await _unitRepository.getAll();

                response.code = "000";
                response.message = "Unidad Eliminada";
                response.data = units;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Delete] Error al eliminar unidad: ", ex.Message);
                throw;
            }
        }

    }
}