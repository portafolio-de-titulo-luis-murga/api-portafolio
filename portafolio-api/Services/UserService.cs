using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using portafolio_api.Models;
using portafolio_api.Models.Dto.User;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;
using portafolio_api.Utils;

namespace portafolio_api.Services
{
    public class UserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly UserRepository _userRepository;
        private readonly IPasswordHasher<CreateUserDto> _passwordHasher;
        private readonly IPasswordHasher<UserRole> _passwordHasher2;
        private readonly DateUtil _dateUtil;
        private readonly SendEmailService _sendEmailService;

        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(
            ILogger<UserService> logger, 
            UserRepository userRepository,
            IPasswordHasher<CreateUserDto> passwordHasher,
            DateUtil dateUtil,
            SendEmailService sendEmailService,
            IHttpContextAccessor httpContextAccessor,
            IPasswordHasher<UserRole> passwordHasher2
        )
        {
            _logger = logger;
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
            _dateUtil = dateUtil;
            _sendEmailService = sendEmailService;
            _httpContextAccessor = httpContextAccessor;
            _passwordHasher2 = passwordHasher2;
        }

        public async Task<GlobalResponse<string>> Create(CreateUserDto createUserDto)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                string fixedPwd = "12345678";

                // Hashear contraseña del usuario a crear
                // string passwordHashed = _passwordHasher.HashPassword(createUserDto, createUserDto.pwd.Trim());
                string passwordHashed = _passwordHasher.HashPassword(createUserDto, fixedPwd);
                _logger.LogInformation("[Create] Se hashea contraseña");

                CrudUser userCrud = new CrudUser()
                {
                    username = createUserDto.username,
                    pwd = passwordHashed,
                    email = createUserDto.email,
                    firstName = createUserDto.firstName,
                    lastName = createUserDto.lastName,
                    idRole = createUserDto.idRole,
                    action = 'I'
                };

                // Llamado a SP para crear usuario
                string resp = await _userRepository.crudUser(userCrud);

                // Validamos codigo de respuesta del procedimiento almacenado
                switch (resp)
                {
                    case "000":
                        _logger.LogInformation("[Create] Usuario creado");
                        string username = userCrud.username;
                        string htmlTemplate = $@"<!DOCTYPE html>
                                                <html>

                                                <head>
                                                <meta charset="" UTF-8"">
                                                <meta name="" viewport"" content="" width=device-width, initial-scale=1"">
                                                </head>

                                                <body>
                                                <div>
                                                    <h3>Tu usuario ha sido creado con éxito.</h3>
                                                    <table>
                                                    <tr>
                                                        <td>
                                                            Hemos generado una contraseña aleatoria, por favor no olvides actualizar dentro del sistema.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tu usuario es: {username}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tu contraseña es: {fixedPwd}</td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        Atte. Sistema de Tarificación.
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </div>
                                                </body>

                                                </html>";
                        _sendEmailService.SendEmail(userCrud.email, "Creación usuario", htmlTemplate);                    
                        break;
                    case "001":
                        _logger.LogInformation("[Create] Nombre de usuario ya existe");
                        throw new CustomErrorsExceptions("Nombre de usuario no valido", 200, "001");
                    case "002":
                        _logger.LogInformation("[Create] Rol no existe");
                        throw new CustomErrorsExceptions("Codigo de rol no valido", 200, "002");
                    default:
                        _logger.LogInformation("[Create] Error al ejecutar creacion de usuario");
                        throw new CustomErrorsExceptions("Codigo de rol no valido", 500, "900");
                }

                response.code = "000";
                response.message = "Usuario creado";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Create] Error al crear usuario", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<GetUserDto>>> GetAll()
        {
            GlobalResponse<List<GetUserDto>> response = new GlobalResponse<List<GetUserDto>>();
            try
            {
                var foundUsers = await _userRepository.getAll();

                response.code = "000";
                response.message = "Usuarios Obtenidos";
                response.data = foundUsers;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAll] Error al obtener todos los usuarios", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<GetUserDto>> GetById(int idUser)
        {
            GlobalResponse<GetUserDto> response = new GlobalResponse<GetUserDto>();
            try
            {
                var foundUser = await _userRepository.getById(idUser);

                if (foundUser == null)
                {
                    _logger.LogInformation("[GetById] Usuario no existe");
                    throw new CustomErrorsExceptions("Codigo de usuario no valido", 200, "001");
                }

                response.code = "000";
                response.message = "Usuario Obtenido";
                response.data = foundUser;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetById] Error al obtener el usuario: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<UserDataReport>> GetPdfReport(int idUser, string dateFilter)
        {
            GlobalResponse<UserDataReport> response = new GlobalResponse<UserDataReport>();
            try
            {
                // Obtengo fecha de inicio y fin a partir de un mes y año (MM/yyyy)
                var dates = _dateUtil.GetDatesOfMonthYear(dateFilter);

                // Llamado a SP
                var foundData = await _userRepository.getPdfReport(dates.initDate, dates.lastDate, idUser);

                if (foundData == null)
                {
                    _logger.LogInformation("[GetPdfReport] Datos no encontrados");
                    throw new CustomErrorsExceptions("Datos no encontrados", 200, "001");
                }

                response.code = "000";
                response.message = "Datos Obtenidos";
                response.data = foundData;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetPdfReport] Datos no encontrados: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<UserReportAnexoExcel>>> GetExcelReport(int idUser, string dateFilter)
        {
            GlobalResponse<List<UserReportAnexoExcel>> response = new GlobalResponse<List<UserReportAnexoExcel>>();
            try
            {
                // Obtengo fecha de inicio y fin a partir de un mes y año (MM/yyyy)
                var dates = _dateUtil.GetDatesOfMonthYear(dateFilter);

                // Llamado a SP
                var resp = await _userRepository.getExcelReport(dates.initDate, dates.lastDate, idUser);

                response.code = "000";
                response.message = "Datos Obtenidos";
                response.data = resp;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetExcelReport] Error buscando datos de reporte excel: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<GetResponsableDto>>> GetAllResponsables()
        {
            GlobalResponse<List<GetResponsableDto>> response = new GlobalResponse<List<GetResponsableDto>>();
            try
            {
                var resp = await _userRepository.getAllResponsable();

                response.code = "000";
                response.message = "Responsables Obtenidos";
                response.data = resp;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAllResponsables] Error al obtener todos los responsables", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<string>> ResetPW(int idUser)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                CreateUserDto userHash = new CreateUserDto();
                string refreshPwd = "12345678";
                string resetPasswordHashed = _passwordHasher.HashPassword(userHash, refreshPwd);
                _logger.LogInformation("[ResetPW] Se ha hasheado la nueva contraseña");

                EmailUser emailUser = await _userRepository.resetPW(idUser, resetPasswordHashed);

                // Enviar correo
                string htmlTemplate = $@"<!DOCTYPE html>
                                        <html>

                                        <head>
                                        <meta charset="" UTF-8"">
                                        <meta name="" viewport"" content="" width=device-width, initial-scale=1"">
                                        </head>

                                        <body>
                                        <div>
                                            <h3>Se ha restablecido tu contraseña.</h3>
                                            <table>
                                            <tr>
                                                <td>
                                                    Hemos generado una contraseña aleatoria, por favor no olvides actualizar dentro del sistema.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Tu contraseña es: {refreshPwd}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                Atte. Sistema de Tarificación.
                                                </td>
                                            </tr>
                                            </table>
                                        </div>
                                        </body>

                                        </html>";
                
                _sendEmailService.SendEmail(emailUser.email, "Restablecimiento de Clave", htmlTemplate);  

                response.code = "000";
                response.message = "Contraseña restablecida correctamente";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[ResetPW] Error al restablecer la contraseña", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<GetUserDto>>> DeleteUser(int idUser)
        {
            GlobalResponse<List<GetUserDto>> response = new GlobalResponse<List<GetUserDto>>();
            try
            {
                string respout = await _userRepository.deleteUser(idUser);

                if (!respout.Equals("000"))
                {
                     _logger.LogInformation("[DeleteUser] Error al eliminar el usuario");
                    throw new CustomErrorsExceptions("Error al eliminar el usuario", 200, "001");
                }

                List<GetUserDto> resp = await _userRepository.getAll();
                
                response.code = "000";
                response.message = "Proveedor eliminado";
                response.data = resp;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[DeleteUser] Error al eliminar usuario: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<GetUserDto>>> DeleteUserWithUnit(int idUser, int idNewUser)
        {
            GlobalResponse<List<GetUserDto>> response = new GlobalResponse<List<GetUserDto>>();
            try
            {
                string coderesp = await _userRepository.deleteUserWithUnit(idUser, idNewUser);

                if (!coderesp.Equals("000"))
                {
                     _logger.LogInformation("[DeleteUserWithUnit] Error al eliminar el usuario");
                    throw new CustomErrorsExceptions("Error al eliminar el usuario", 200, "001");
                }

                List<GetUserDto> resp = await _userRepository.getAll();
                
                response.code = "000";
                response.message = "Usuario con unidad eliminado";
                response.data = resp;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[DeleteUserWithUnit] Error al eliminar proveedor: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<string>> UpdateUser(UpdateUser updateUser, int idUser)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                string resp = await _userRepository.updateUser(updateUser, idUser);

                if (!resp.Equals("000"))
                {
                    _logger.LogInformation("[UpdateUser] Usuario no existe");
                    throw new CustomErrorsExceptions("No se actualiza el usuario", 200, "001");
                }

                response.code = "000";
                response.message = "Usuario Actualizo";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[UpdateUser] Error al actualizar el usuario", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<string>> UpdatePwd(UpdatePwdDto updatePwdDto)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                // Buscamos dentro del JWT si viene el id del usuario
                var user = _httpContextAccessor.HttpContext.User;
                int idUser = 0;
                if (user != null) {
                    var claimId = user.Claims.FirstOrDefault(c => c.Type == "Id");
                    idUser = int.Parse(claimId.Value);
                }

                var foundUser = await _userRepository.getById(idUser);

                if (foundUser == null)
                {
                    _logger.LogInformation("[GetById] Usuario no existe");
                    throw new CustomErrorsExceptions("No se actualiza la contraseña", 200, "001");
                }

                //Validacion nombre de usuario
                UserRole? userFound = await _userRepository.getUserByUsername(foundUser.userName);
    
                // Validacion de contraseña
                PasswordVerificationResult verifyPassword = _passwordHasher2.VerifyHashedPassword(userFound, userFound.pwdUser, updatePwdDto.oldPwd);

                if (verifyPassword != PasswordVerificationResult.Success)
                {
                    _logger.LogInformation("[UpdatePwd] Constraseña no coincide");
                    throw new CustomErrorsExceptions("Constraseña no coincide", 200, "002");
                }

                string passwordHashed = _passwordHasher2.HashPassword(userFound, updatePwdDto.newPwd);
                _logger.LogInformation("[Create] Se hashea contraseña");

                await _userRepository.updatePwd(idUser, passwordHashed);

                 string htmlTemplate = $@"<!DOCTYPE html>
                                        <html>

                                        <head>
                                        <meta charset="" UTF-8"">
                                        <meta name="" viewport"" content="" width=device-width, initial-scale=1"">
                                        </head>

                                        <body>
                                        <div>
                                            <h3>Has cambiado tu contraseña.</h3>
                                            <table>
                                            <tr>
                                                <td>
                                                    Tu contraseña se ha cambiado con éxito!
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                Atte. Sistema de Tarificación.
                                                </td>
                                            </tr>
                                            </table>
                                        </div>
                                        </body>

                                        </html>";
                
                _sendEmailService.SendEmail(userFound.email, "Cambio de Clave", htmlTemplate); 

                response.code = "000";
                response.message = "Contraseña Actualizada";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[UpdatePwd] Error al actualizar contraseña: ", ex.Message);
                throw;
            }
        }
        
    }
}