using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;

namespace portafolio_api.Services
{
    public class SendEmailService
    {
        private readonly IConfiguration _configuration;

        public SendEmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string SendEmail(string to, string subject, string htmlTemplate, byte[] base64Pdf = null, string pdfName = null)
        {
            try
            {
                var emailConfig = _configuration.GetSection("EmailSettings");

                MimeMessage email = new MimeMessage();
                email.From.Add(MailboxAddress.Parse(emailConfig.GetValue<string>("From")));
                email.To.Add(MailboxAddress.Parse(to));
                email.Subject = subject;

                var bodyHtml = new TextPart(TextFormat.Html) { Text = htmlTemplate };

                MemoryStream memoryStream = null;
                MimePart attachments = null;
                if (base64Pdf != null)
                {
                    memoryStream = new MemoryStream(base64Pdf);
                    attachments = new MimePart("application/pdf") {
                        Content = new MimeContent(memoryStream, ContentEncoding.Default),
                        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                        ContentTransferEncoding = ContentEncoding.Base64,
                        FileName = pdfName
                    };
                }
                

                var multipart = new Multipart("mixed");
                multipart.Add(bodyHtml);
                if (base64Pdf != null)
                {
                    multipart.Add(attachments);
                }

                email.Body = multipart;

                using var smtp = new SmtpClient();
                smtp.Connect(emailConfig.GetValue<string>("Connect"), emailConfig.GetValue<int>("PortConnect"), SecureSocketOptions.StartTls);
                smtp.Authenticate(emailConfig.GetValue<string>("From"), emailConfig.GetValue<string>("ClvFrom"));
                smtp.Send(email);
                smtp.Disconnect(true);

                if (base64Pdf != null)
                {
                    memoryStream.Close();
                    memoryStream.Dispose();
                }

                return "Correo enviado";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}