using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using portafolio_api.Models;
using portafolio_api.Models.Data;
using portafolio_api.Repositories;
using portafolio_api.Utils;

namespace portafolio_api.Services
{
    public class TokenService
    {
        private readonly ILogger<TokenService> _logger;
        private readonly IConfiguration _configuration;
        private readonly UserSessionRepository _userSessionRepository;
        private readonly MethodsUtil _methodsUtil;

        public TokenService(
            ILogger<TokenService> logger, 
            IConfiguration configuration,
            UserSessionRepository userSessionRepository,
            MethodsUtil methodsUtil
        )
        {
            _logger = logger;
            _configuration = configuration;
            _userSessionRepository = userSessionRepository;
            _methodsUtil = methodsUtil;
        }

        public TokenLogin CreateToken(UserRole user)
        {
            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
    
            // Se obtienen variables de entorno
            var jwtSetting = _configuration.GetSection("JwtSettings");
            string keySecret = jwtSetting.GetValue<string>("SecretKey");
            int minuteExpire = jwtSetting.GetValue<int>("MinuteToExpiration");

            byte[] key = Encoding.ASCII.GetBytes(keySecret);

            // Configuración del Token
            SecurityTokenDescriptor securityTokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("Id", user.idUser.ToString()),
                    new Claim(JwtRegisteredClaimNames.Name, user.userName),
                    new Claim(ClaimTypes.Email, user.email),
                    new Claim(ClaimTypes.Role, user.roleName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(minuteExpire),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            // Creación del Token
            
            SecurityToken securityToken = jwtSecurityTokenHandler.CreateToken(securityTokenDescriptor);
            string jwtToken = jwtSecurityTokenHandler.WriteToken(securityToken);
            _logger.LogInformation("[CreateToken] Se crea un Token de autorización");

            UserSessions userSessions = new UserSessions()
            {
                refreshToken = _methodsUtil.RandomString(50) + Guid.NewGuid(),
                jwtId = securityToken.Id,
                idUser = user.idUser
            };

            _userSessionRepository.createSessionToken(userSessions);
            _logger.LogInformation("[CreateToken] Se crea refresh token");

            TokenLogin tokenLogin = new TokenLogin()
            {
                token = jwtToken,
                refreshToken = userSessions.refreshToken
            };

            return tokenLogin;
        }
    }
}