using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using portafolio_api.Models.Dto.Service;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;

namespace portafolio_api.Services
{
    public class ServiceService
    {
        private readonly ILogger<ServiceService> _logger;
        private readonly ServiceRepository _serviceRepository;

        public ServiceService(
            ILogger<ServiceService> logger,
            ServiceRepository serviceRepository
        )
        {
            _logger = logger;
            _serviceRepository = serviceRepository;
        }

        public async Task<GlobalResponse<List<GetServiceDto>>> GetAllService()
        {
            GlobalResponse<List<GetServiceDto>> response = new GlobalResponse<List<GetServiceDto>>();
            try
            {
                var services = await _serviceRepository.getAll();

                response.code = "000";
                response.message = "Servicios Obtenidos";
                response.data = services;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAllService] Error al obtener todos los servicios", ex.Message);
                throw;
            }
        }

    }
}