using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using portafolio_api.Models.Dto.Role;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;

namespace portafolio_api.Services
{
    public class RoleService
    {
        private readonly ILogger<RoleService> _logger;
        private readonly RoleRepository _roleRepository;

        public RoleService(ILogger<RoleService> logger, RoleRepository roleRepository)
        {
            _logger = logger;
            _roleRepository = roleRepository;
        }

        public async Task<GlobalResponse<List<GetRoleDto>>> GetAll()
        {
            GlobalResponse<List<GetRoleDto>> response = new GlobalResponse<List<GetRoleDto>>();
            try
            {
                var foundRoles = await _roleRepository.getAll();

                response.code = "000";
                response.message = "Roles Obtenidos";
                response.data = foundRoles;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAll] Error al obtener todos los roles", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<GetRoleDto>> GetById(int idRole)
        {
            GlobalResponse<GetRoleDto> response = new GlobalResponse<GetRoleDto>();
            try
            {
                var foundRole = await _roleRepository.getById(idRole);

                if (foundRole == null)
                {
                    _logger.LogInformation("[GetById] Role no existe");
                    throw new CustomErrorsExceptions("Codigo de role no valido", 200, "001");
                }

                response.code = "000";
                response.message = "Role Obtenido";
                response.data = foundRole;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetById] Error al obtener el role", ex.Message);
                throw;
            }
        }
    }
}