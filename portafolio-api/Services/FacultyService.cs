using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using portafolio_api.Models.Dto.Faculty;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;
using portafolio_api.Utils;

namespace portafolio_api.Services
{
    public class FacultyService
    {
        private readonly ILogger<FacultyService> _logger;
        private readonly FacultyRepository _facultyRepository;
        private readonly DateUtil _dateUtil;

        public FacultyService(ILogger<FacultyService> logger, FacultyRepository facultyRepository, DateUtil dateUtil)
        {
            _logger = logger;
            _facultyRepository = facultyRepository;
            _dateUtil = dateUtil;
        }

        public async Task<GlobalResponse<List<GetFacultyDto>>> GetAll()
        {
            GlobalResponse<List<GetFacultyDto>> response = new GlobalResponse<List<GetFacultyDto>>();
            try
            {
                var foundFaculty = await _facultyRepository.getAll();

                response.code = "000";
                response.message = "Facultades Obtenidas";
                response.data = foundFaculty;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAll] Error al obtener todas las facultades", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<GetFacultyDto>> GetById(int idFaculty)
        {
            GlobalResponse<GetFacultyDto> response = new GlobalResponse<GetFacultyDto>();
            try
            {
                var foundFaculty = await _facultyRepository.getById(idFaculty);

                if (foundFaculty == null)
                {
                    _logger.LogInformation("[GetById] Facultad no existe");
                    throw new CustomErrorsExceptions("Codigo de facultad no valido", 200, "001");
                }

                response.code = "000";
                response.message = "Facultad Obtenida";
                response.data = foundFaculty;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetById] Error al obtener facultad: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<ReportAllUniversityDto>> GetReportAllUniversity(string dateFilter)
        {
            GlobalResponse<ReportAllUniversityDto> response = new GlobalResponse<ReportAllUniversityDto>();
            try
            {
                var dates = _dateUtil.GetDatesOfMonthYear(dateFilter);
                var reportUniversity = await _facultyRepository.getReportAllUniversity(dates.initDate, dates.lastDate);

                response.code = "000";
                response.message = "Reporte Universidad";
                response.data = reportUniversity;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetReportAllUniversity] Error al obtener reporte de universidad: ", ex.Message);
                throw;
            }
        }


        public async Task<GlobalResponse<GetCountAll>> GetAllCount()
        {
            GlobalResponse<GetCountAll> response = new GlobalResponse<GetCountAll>();
            try
            {
                var f = await _facultyRepository.getAllCount();

                response.code = "000";
                response.message = "contadores Obtenidos";
                response.data = f;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAllCount] Error al obtener todos los contadores", ex.Message);
                throw;
            }
        }

    }
}