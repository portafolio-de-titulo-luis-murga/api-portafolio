using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using portafolio_api.Models.Data;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;
using portafolio_api.Utils;

namespace portafolio_api.Services
{
    public class ReportService
    {
        private readonly ILogger<ReportService> _logger;
        private readonly ReportRepository _reportRepository;
        private readonly AnexoRepository _anexoRepository;
        private readonly SendEmailService _sendEmailService;
        private readonly UnitRepository _unitRepository;
        private readonly UserRepository _userRepository;
        private readonly FacultyRepository _facultyRepository;
        private readonly DateUtil _dateUtil;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ReportService(
            ILogger<ReportService> logger, 
            ReportRepository reportRepository, 
            AnexoRepository anexoRepository,
            SendEmailService sendEmailService,
            IHttpContextAccessor httpContextAccessor,
            UnitRepository unitRepository,
            UserRepository userRepository,
            FacultyRepository facultyRepository,
            DateUtil dateUtil
        )
        {
            _logger = logger;
            _reportRepository = reportRepository;
            _anexoRepository = anexoRepository;
            _sendEmailService = sendEmailService;
            _httpContextAccessor = httpContextAccessor;
            _unitRepository = unitRepository;
            _userRepository = userRepository;
            _dateUtil = dateUtil;
            _facultyRepository = facultyRepository;
        }

        public async Task<GlobalResponse<ResponseGeneratePdf>> ValidateAndGeneratePdf(JsonElement generic)
        {
            GlobalResponse<ResponseGeneratePdf> response = new GlobalResponse<ResponseGeneratePdf>();
            try
            {
                var genericObj = JsonSerializer.Deserialize<DataReportPdf<Dictionary<string, JsonElement>>>(generic.GetRawText());
                
                // Buscamos dentro del JWT si viene el id del usuario
                var user = _httpContextAccessor.HttpContext.User;
                string userEmail = "";
                if (user != null) {
                    var claimId = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
                    userEmail = claimId.Value;
                }

                // Validamos si existen las siguientes variables
                string initDate = genericObj.data.ContainsKey("initDate") ? genericObj.data["initDate"].GetString() : null;
                string lastDate = genericObj.data.ContainsKey("lastDate") ? genericObj.data["lastDate"].GetString() : null;

                switch (genericObj.idTemplate)
                {
                    case "RPANEXO":
                        int idAnexo = genericObj.data["idAnexo"].GetInt32();
                        var anexo = await _anexoRepository.getAllReport(initDate, lastDate, idAnexo, true);

                        if (anexo == null)
                        {
                            _logger.LogInformation("[ValidateAndGeneratePdf] Anexo no existe");
                            throw new CustomErrorsExceptions("Anexo no existe", 200, "001");
                        }

                        var anexoDataJson = JsonSerializer.SerializeToDocument(anexo).RootElement;
                        genericObj.data["dataOfAnexo"] = anexoDataJson;
                        break;
                    case "RPALLANEXOS":
                        var reportAllAnexos = await _anexoRepository.calcTotalTarification(initDate, lastDate);
                        var anexosList = await _anexoRepository.getAllReport(initDate, lastDate, 0, false);
                        var reportAllAnexosJson = JsonSerializer.SerializeToDocument(reportAllAnexos).RootElement;
                        var anexosListJson = JsonSerializer.SerializeToDocument(anexosList).RootElement;
                        genericObj.data["reportAllAnexos"] = reportAllAnexosJson;
                        genericObj.data["anexosList"] = anexosListJson;
                        break;
                    case "RPUNIT":
                        int idUnit = genericObj.data["idUnit"].GetInt32();
                        var unit = await _unitRepository.getUnitReport(initDate, lastDate, idUnit, true);

                        if (unit == null)
                        {
                            _logger.LogInformation("[ValidateAndGeneratePdf] Anexo no existe");
                            throw new CustomErrorsExceptions("Anexo no existe", 200, "001");
                        }

                        var unitDataJson = JsonSerializer.SerializeToDocument(unit).RootElement;
                        genericObj.data["dataOfUnit"] = unitDataJson;
                        break;
                    case "RPALLUNITS":
                        var reportAllUnits = await _unitRepository.calcTotalTarification(initDate, lastDate);
                        var unitsList = await _unitRepository.getUnitReport(initDate, lastDate, 0, false);
                        var reportAllUnitsJson = JsonSerializer.SerializeToDocument(reportAllUnits).RootElement;
                        var unitsListJson = JsonSerializer.SerializeToDocument(unitsList).RootElement;
                        genericObj.data["reportAllUnits"] = reportAllUnitsJson;
                        genericObj.data["unitsList"] = unitsListJson;
                        break;
                    case "RPRESPONSABLE":
                        int idUserSupervisor = genericObj.data["idUserSupervisor"].GetInt32();
                        string dateMonthYear = genericObj.data["dateMonthYear"].GetString();
                        var dates = _dateUtil.GetDatesOfMonthYear(dateMonthYear);
                        var foundData = await _userRepository.getPdfReport(dates.initDate, dates.lastDate, idUserSupervisor);

                        if (foundData == null)
                        {
                            _logger.LogInformation("[ValidateAndGeneratePdf] Datos no encontrados");
                            throw new CustomErrorsExceptions("Datos no encontrados", 200, "001");
                        }
                        var foundDataJson = JsonSerializer.SerializeToDocument(foundData).RootElement;
                        genericObj.data["dataReport"] = foundDataJson;
                        break;
                    case "RPUNIVERSITY":
                        string monthYear = genericObj.data["dateMonthYear"].GetString();
                        var datesReport = _dateUtil.GetDatesOfMonthYear(monthYear);
                        var dataReport = await _facultyRepository.getReportAllUniversity(datesReport.initDate, datesReport.lastDate);
                        var dataReportJson = JsonSerializer.SerializeToDocument(dataReport).RootElement;
                        genericObj.data["dataReport"] = dataReportJson;
                        break;
                }

                ResponseGeneratePdf respPdf = await _reportRepository.GeneratePdf(genericObj);

                // Validamos con un diccionario los templates que envian correo
                var emailDictionary = new Dictionary<string, string>();
                emailDictionary.Add("RPALLANEXOS", "ReporteTotalAnexos");
                emailDictionary.Add("RPALLUNITS", "ReporteTotalUnidades");
                emailDictionary.Add("RPRESPONSABLE", "ReporteResponsable");
                emailDictionary.Add("RPUNIVERSITY", "ReporteUniversidad");
                string namePdfEmail;
                if (emailDictionary.TryGetValue(genericObj.idTemplate, out namePdfEmail)) 
                {
                    string htmlTemplate = @"<!DOCTYPE html>
                        <html>
                        <head>
                        <meta charset=""UTF-8"">
                        <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
                        </head>
                        <body>
                        <div>
                            <h3>Has generado un reporte del sistema de Tarificación</h3>
                            <table>
                            <tr>
                                <td>
                                Se adjunta documento generado.
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                Atte. Sistema de Tarificación.
                                </td>
                            </tr>
                            </table>
                        </div>
                        </body>
                        </html>";
                    _sendEmailService.SendEmail(userEmail, "Notificación de Reporte", htmlTemplate, respPdf.pdfFile, namePdfEmail);
                }
                
                response.code = "000";
                response.message = "Generación de PDF OK";
                response.data = respPdf;
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}