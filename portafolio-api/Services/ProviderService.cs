﻿using Microsoft.AspNetCore.Identity;
using portafolio_api.Models;
using portafolio_api.Models.Dto.Provider;
using portafolio_api.Models.Dto.User;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;

namespace portafolio_api.Services
{
    public class ProviderService
    {
        private readonly ILogger<ProviderService> _logger;
        private readonly ProviderRepository _providerRepository;

        public ProviderService(
            ILogger<ProviderService> logger,
            ProviderRepository providerRepository
        )
        {
            _logger = logger;
            _providerRepository = providerRepository;
        }

        public async Task<GlobalResponse<string>> Create(CreateProviderDto createProviderDto)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                CreateProvider providerCreate = new CreateProvider()
                {
                    nameProvider = createProviderDto.nameProvider,
                    fixedCost = createProviderDto.fixedCost
                };

                // Llamado a SP para crear provider
                ResponseCreateProvider resp = await _providerRepository.createProvider(providerCreate);

                if (!resp.codeRespSP.Equals("000"))
                {
                    _logger.LogInformation("[Create] Nombre de proveedor ya existe");
                    throw new CustomErrorsExceptions("Nombre de proveedor no valido", 200, "001");
                }

                foreach (CostServiceCreateProvider service in createProviderDto.services)
                {
                    CrudCostService costService = new CrudCostService()
                    {
                        idService = service.idService,
                        costService = service.costService,
                        idProvider = resp.idProvider,
                        action = 'I'
                    };
                    // Llamado a SP para crear costos de servicios asociados a un proveedor
                    await _providerRepository.crudCostService(costService);
                }

                response.code = "000";
                response.message = "Proveedor Creado";
                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<GlobalResponse<List<GetProviderDto>>> GetAllProviders()
        {
            GlobalResponse<List<GetProviderDto>> response = new GlobalResponse<List<GetProviderDto>>();
            try
            {
                var providers = await _providerRepository.getAll();

                response.code = "000";
                response.message = "Proveedores Obtenidos";
                response.data = providers;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAllProviders] Error al obtener todos los proveedores", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<GetProviderDto>> GetId(int idProvider)
        {
            GlobalResponse<GetProviderDto> response = new GlobalResponse<GetProviderDto>();
            try
            {
                var foundProvider = await _providerRepository.getProviderId(idProvider);

                if (foundProvider == null)
                {
                    _logger.LogInformation("[GetId] Proveedor no encontrado");
                    throw new CustomErrorsExceptions("Proveedor no encontrado", 200, "001");
                }

                response.code = "000";
                response.message = "Proveedor Obtenido";
                response.data = foundProvider;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetId] Error al obtener proveedor", ex.Message);
                throw;
            }
        }


        public async Task<GlobalResponse<List<GetProviderDto>>> Delete(int providerId)
        {
            GlobalResponse<List<GetProviderDto>> response = new GlobalResponse<List<GetProviderDto>>();
            try
            {
                string respout = await _providerRepository.deleteProvider(providerId);

                if (!respout.Equals("000"))
                {
                     _logger.LogInformation("[Delete] Error al eliminar el proveedor");
                    throw new CustomErrorsExceptions("Error al eliminar el proveedor", 200, "001");
                }

                List<GetProviderDto> getproviders = await _providerRepository.getAll();
                
                response.code = "000";
                response.message = "Proveedor eliminado";
                response.data = getproviders;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Delete] Error al eliminar proveedor: ", ex.Message);
                throw;
            }
        }
        public async Task<GlobalResponse<List<GetProviderDto>>> DeleteWithFaculty(int providerId, int providerIdWithFaculty)
        {
            GlobalResponse<List<GetProviderDto>> response = new GlobalResponse<List<GetProviderDto>>();
            try
            {
                string coderesp = await _providerRepository.deleteProviderWithFaculty(providerId, providerIdWithFaculty);

                if (!coderesp.Equals("000"))
                {
                     _logger.LogInformation("[DeleteWithFaculty] Error al eliminar el proveedor");
                    throw new CustomErrorsExceptions("Error al eliminar el proveedor", 200, "001");
                }

                List<GetProviderDto> getProvidersWithFaculty = await _providerRepository.getAll();
                
                response.code = "000";
                response.message = "Proveedor con facultad eliminado";
                response.data = getProvidersWithFaculty;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[DeleteWithFaculty] Error al eliminar proveedor: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<string>> UpdateProvider(UpdateProviderDto updateProviderDto)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                string resp = await _providerRepository.updateProvider(updateProviderDto);

                if (resp.Equals("004"))
                {
                    _logger.LogInformation("[updateProvider] Nombre de Proveedor ya está utilizado");
                    throw new CustomErrorsExceptions("Nombre Proveedor ya existe", 200, "001");
                }

                if (!resp.Equals("000"))
                {
                    _logger.LogInformation("[updateProvider] Valores id inconsistentes");
                    throw new CustomErrorsExceptions("No se actualiza", 200, "002");
                }

                response.code = "000";
                response.message = "Proveedor Actualizo";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[UpdateProvider] Error al actualizar el proveedor", ex.Message);
                throw;
            }
        }
    }
}
