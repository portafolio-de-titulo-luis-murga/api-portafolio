using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using portafolio_api.Models.Dto.Anexos;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;

namespace portafolio_api.Services
{
    public class AnexoService
    {
        private readonly ILogger<ServiceService> _logger;
        private readonly AnexoRepository _anexoRepository;

        public AnexoService(
            ILogger<ServiceService> logger,
            AnexoRepository anexoRepository
        )
        {
            _logger = logger;
            _anexoRepository = anexoRepository;
        }

        public async Task<GlobalResponse<List<GetAnexoDto>>> GetAllAnexos()
        {
            GlobalResponse<List<GetAnexoDto>> response = new GlobalResponse<List<GetAnexoDto>>();
            try
            {
                var anexos = await _anexoRepository.getAll();

                response.code = "000";
                response.message = "Anexos Obtenidos";
                response.data = anexos;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAllAnexos] Error al obtener todos los anexos: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<GetAnexoDto>> GetById(int idAnexo)
        {
            GlobalResponse<GetAnexoDto> response = new GlobalResponse<GetAnexoDto>();
            try
            {
                var foundAnexo = await _anexoRepository.getById(idAnexo);

                if (foundAnexo == null)
                {
                    _logger.LogInformation("[GetById] Anexo no encontrado");
                    throw new CustomErrorsExceptions("Anexo no encontrado", 200, "001");
                }

                response.code = "000";
                response.message = "Anexo Obtenido";
                response.data = foundAnexo;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetById] Error al obtener anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<string>> Create(CreateUpdateAnexoDto createUpdateAnexoDto)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                string codeResp = await _anexoRepository.create(createUpdateAnexoDto);

                if (codeResp.Equals("001"))
                {
                    _logger.LogInformation("[Create] Anexo ya existe");
                    throw new CustomErrorsExceptions("Anexo ya existe", 200, "001");
                } else if (codeResp.Equals("002"))
                {
                    _logger.LogInformation("[Create] Unidad no encontrada");
                    throw new CustomErrorsExceptions("Unidad no encontrada", 200, "002");
                }

                response.code = "000";
                response.message = "Anexo Creado";
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Create] Error al crear anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<GetAnexoDto>>> Update(CreateUpdateAnexoDto createUpdateAnexoDto)
        {
            GlobalResponse<List<GetAnexoDto>> response = new GlobalResponse<List<GetAnexoDto>>();
            try
            {
                string codeResp = await _anexoRepository.update(createUpdateAnexoDto);

                if (codeResp.Equals("001"))
                {
                    _logger.LogInformation("[Update] Anexo no existe");
                    throw new CustomErrorsExceptions("Anexo no existe", 200, "001");
                } else if (codeResp.Equals("002"))
                {
                    _logger.LogInformation("[Update] Unidad no encontrada");
                    throw new CustomErrorsExceptions("Unidad no encontrada", 200, "002");
                }

                var anexos = await _anexoRepository.getAll();

                response.code = "000";
                response.message = "Anexo Actualizado";
                response.data = anexos;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Update] Error al actualizar anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<List<GetAnexoDto>>> UpdateStatus(int idAnexo)
        {
            GlobalResponse<List<GetAnexoDto>> response = new GlobalResponse<List<GetAnexoDto>>();
            try
            {
                string codeResp = await _anexoRepository.updateStatus(idAnexo);

                if (codeResp.Equals("001"))
                {
                    _logger.LogInformation("[UpdateStatus] Anexo no existe");
                    throw new CustomErrorsExceptions("Anexo no existe", 200, "001");
                }

                var anexos = await _anexoRepository.getAll();

                response.code = "000";
                response.message = "Anexo Actualizado";
                response.data = anexos;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[UpdateStatus] Error al actualizar anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<object>> GetReportAnexos(string initDate, string lastDate)
        {
            GlobalResponse<object> response = new GlobalResponse<object>();
            try
            {
                var anexos = await _anexoRepository.getAllReport(initDate, lastDate, 0, false);

                response.code = "000";
                response.message = "Anexos Obtenidos";
                response.data = anexos;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetReportAnexos] Error al obtener todos los anexos: ", ex.Message);
                throw;
            }
        }


        public async Task<GlobalResponse<List<GetAnexoBySupervisorDto>>> GetAnexoBySupervisor(int idUser)
        {
            GlobalResponse<List<GetAnexoBySupervisorDto>> response = new GlobalResponse<List<GetAnexoBySupervisorDto>>();
            try
            {
                var resp = await _anexoRepository.getAnexoBySupervisor(idUser);

                response.code = "000";
                response.message = "Responsable Obtenido";
                response.data = resp;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetAnexoBySupervisor] Error al obtener el responsable", ex.Message);
                throw;
            }
        }


        public async Task<GlobalResponse<List<GetDstByAnexoDto>>> GetDstByAnexo(int idProvider, string initDate, string lastDate)
        {
            GlobalResponse<List<GetDstByAnexoDto>> response = new GlobalResponse<List<GetDstByAnexoDto>>();
            try
            {
                var resp = await _anexoRepository.getDstByAnexo(idProvider, initDate, lastDate);

                if (resp == null)
                {
                    _logger.LogInformation("[GetDstByAnexo] Dst del anexo no encontrado");
                    throw new CustomErrorsExceptions("Dst del anexo no encontrado", 200, "001");
                }

                response.code = "000";
                response.message = "Dst del anexo Obtenido";
                response.data = resp;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetDstByAnexo] Error al obtener dst del anexo", ex.Message);
                throw;
            }
        }

        public async Task<GlobalResponse<GetCostByAnexoUserDto>> GetCostByAnexoUser(GetAnexoService getAnexoService)
        {
            GlobalResponse<GetCostByAnexoUserDto> response = new GlobalResponse<GetCostByAnexoUserDto>();
            try
            {
                var resp = await _anexoRepository.getCostByAnexoUser(getAnexoService);

                if (resp == null)
                {
                    _logger.LogInformation("[GetCostByAnexoUser] costo del servicio en el anexo no encontrado");
                    throw new CustomErrorsExceptions("No encontrado", 200, "001");
                }

                response.code = "000";
                response.message = "Obtenido";
                response.data = resp;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[GetCostByAnexoUser] Error al obtener costo del servicio en el anexo no encontrado", ex.Message);
                throw;
            }
        }
    }
}