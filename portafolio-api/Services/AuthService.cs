﻿using Microsoft.AspNetCore.Identity;
using portafolio_api.Models;
using portafolio_api.Models.Dto.Auth;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;

namespace portafolio_api.Services
{
    public class AuthService
    {
        //todo los parametros del copnstructor se debn definir 
        private readonly ILogger<AuthService> _logger;
        private readonly UserRepository _userRepository;
        private readonly IPasswordHasher<UserRole> _passwordHasher;
        private readonly TokenService _tokenService;
        private readonly UserSessionRepository _userSessionRepository;

        //constructor del servicio
        public AuthService(
            ILogger<AuthService> logger, 
            UserRepository userRepository, 
            IPasswordHasher<UserRole> passwordHasher,
            TokenService tokenService,
            UserSessionRepository userSessionRepository
        ) 
        { 
            _logger = logger;
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
            _tokenService = tokenService;
            _userSessionRepository = userSessionRepository;
        }

        public async Task<GlobalResponse<LoginResponseDto>> Login(LoginDto loginDto) 
        {
            try
            {
                GlobalResponse<LoginResponseDto> response = new GlobalResponse<LoginResponseDto>();
                
                //Validacion nombre de usuario
                UserRole? userFound = await _userRepository.getUserByUsername(loginDto.username);

                if (userFound == null)
                {
                    _logger.LogInformation("[Login] Usuario no encontrado");
                    throw new CustomErrorsExceptions("Usuario o contraseña incorrectos", 200, "001");
                }

                // Validacion de contraseña
                PasswordVerificationResult verifyPassword = _passwordHasher.VerifyHashedPassword(userFound, userFound.pwdUser, loginDto.pwd);

                if (verifyPassword != PasswordVerificationResult.Success)
                {
                    _logger.LogInformation("[Login] Constraseña no coincide");
                    throw new CustomErrorsExceptions("Usuario o contraseña incorrectos", 200, "001");
                }

                _userSessionRepository.findTokenAndDelete(userFound.idUser);

                // Creación de Token (jwt)
                TokenLogin tokenLogin = _tokenService.CreateToken(userFound);
                LoginResponseDto loginResponseDto = new LoginResponseDto
                {
                    token = tokenLogin.token,
                    refreshToken = tokenLogin.refreshToken
                };

                response.code = "000";
                response.message = "Login exitoso";
                response.data = loginResponseDto;

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("[Login] Error al tratar de realizar login", ex.Message);
                throw;
            }
        }
    }
}
