using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Unit
{
    public class UnitsReportDto
    {
        public int idUnit { get; set; }
        public string unitName { get; set; }
        public string userName { get; set; }
        public string facultyName { get; set; }
        public string nameProvider { get; set; }
        public int countAnexos { get; set; }
        public int fixedCostAnexos { get; set; }
        public int? CELTotalSeg { get; set; }
        public int? CELTotalCost { get; set; }
        public int? CELCallQuantity { get; set; }
        public int? CELCostService { get; set; }
        public int? SLMTotalSeg { get; set; }
        public int? SLMTotalCost { get; set; }
        public int? SLMCallQuantity { get; set; }
        public int? SLMCostService { get; set; }
        public int? LDITotalSeg { get; set; }
        public int? LDITotalCost { get; set; }
        public int? LDICallQuantity { get; set; }
        public int? LDICostService { get; set; }
        public int? totalSeg { get; set; } = 0;
        public int? totalCost { get; set; } = 0;
        public int? CallQuantity { get; set; } = 0;
    }
}