using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Unit
{
    public class GetUnitServiceReport
    {
        public int idService { get; set; }
        public string nameService { get; set; }
        public int spokenSeconds { get; set; } = 0;
        public int cost { get; set; } = 0;
        public int numberOfAnsweredCalls { get; set; } = 0;
        public int costService { get; set; }
    }
}