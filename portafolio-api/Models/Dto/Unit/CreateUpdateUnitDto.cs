using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Unit
{
    public class CreateUpdateUnitDto
    {
        public string unitName { get; set; }
        public int idFaculty { get; set; }
        public int idUser { get; set; }
    }
}