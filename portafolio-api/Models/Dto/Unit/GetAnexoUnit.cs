using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Unit
{
    public class GetAnexoUnit
    {
        public int idAnexo { get; set; }
    }
}