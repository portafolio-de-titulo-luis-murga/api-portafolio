using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Unit
{
    public class GetUnitReport
    {
        public int idUnit { get; set; }
        public string unitName { get; set; }
        public string userName { get; set; }
        public string facultyName { get; set; }
        public string nameProvider { get; set; }
        public int countAnexos { get; set; }
        public int fixedCostAnexos { get; set; }
    }
}