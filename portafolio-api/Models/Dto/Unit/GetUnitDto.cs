using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Unit
{
    public class GetUnitDto
    {
        public GetUnitDto()
        {
            anexos = new List<GetAnexoUnit>();
        }
        public int idUnit { get; set; }
        public string unitName { get; set; } ="";
        public int idFaculty { get; set; }
        public string facultyName { get; set; } = "";
        public int idUser { get; set; }
        public string nameUser { get; set; } = "";
        public int numberOfAnexo { get; set; } = 0;
        public List<GetAnexoUnit> anexos { get; set; }
    }
}