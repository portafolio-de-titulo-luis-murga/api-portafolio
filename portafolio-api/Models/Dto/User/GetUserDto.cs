using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.User
{
    public class GetUserDto
    {
        public int idUser { get; set; }
        public string userName { get; set; } = "";
        public string firstName { get; set; } = "";
        public string lastName { get; set; } = "";
        public string email { get; set; } = "";
        public int idRole { get; set; }
        public string roleName { get; set; } = "";
        public int hasRelationUnits { get; set; }

    }
}