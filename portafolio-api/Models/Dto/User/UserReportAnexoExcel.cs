using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.User
{
    public class UserReportAnexoExcel
    {
        public string userName { get; set; }
        public string unitName { get; set; }
        public int idAnexo { get; set; }
        public int slmTotalSeg { get; set; } = 0;
        public int slmTotalCost { get; set; } = 0;
        public int slmTotalCall { get; set; } = 0;
        public int celTotalSeg { get; set; } = 0;
        public int celTotalCost { get; set; } = 0;
        public int celTotalCall { get; set; } = 0;
        public int ldiTotalSeg { get; set; } = 0;
        public int ldiTotalCost { get; set; } = 0;
        public int ldiTotalCall { get; set; } = 0;
    }
}