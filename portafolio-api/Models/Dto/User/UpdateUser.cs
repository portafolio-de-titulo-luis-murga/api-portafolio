using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.User
{
    public class UpdateUser
    {
        public string firstName { get; set; } ="";
        public string lastName { get; set; } = "";
        public string email { get; set; }   = "";
    }
}