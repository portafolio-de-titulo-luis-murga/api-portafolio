using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using portafolio_api.Models.Dto.Unit;

namespace portafolio_api.Models.Dto.User
{
    public class UserDataReport
    {
        public UserDataReport()
        {
            services = new List<GetUnitServiceReport>();
        }
    
        public int idUser { get; set; }
        public string userName { get; set; } = null!;
        public int cantUnidades { get; set; } = 0;
        public int cantAnexos { get; set; } = 0;
        public int countCall { get; set; } = 0;
        public int totalCallTime { get; set; } = 0;
        public int totalCost { get; set; } = 0;
        public List<GetUnitServiceReport> services { get; set; }
    }
}