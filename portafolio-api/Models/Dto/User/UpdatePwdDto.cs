using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.User
{
    public class UpdatePwdDto
    {
        public string oldPwd { get; set; }
        public string newPwd { get; set; }
    }
}