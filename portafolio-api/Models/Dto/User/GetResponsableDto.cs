using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.User
{
    public class GetResponsableDto
    {
        public int idUser { get; set; }
        public string nameResponsable { get; set; } = "";
    }
}