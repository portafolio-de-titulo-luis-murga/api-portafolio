using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Anexos
{
    public class GetAnexoDto
    {
        public int idAnexo { get; set; }
        public int isDeleted { get; set; }
        public int idUnit { get; set; }
        public string unitName { get; set; }
    }
}