using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Anexos
{
    public class GetCostByAnexoUserDto
    {
        public GetCostByAnexoUserDto()
        {
            services = new List<DataAnexoService>();
        }
        public int idAnexo { get; set; }
        public List<DataAnexoService> services { get; set; }

    }
}