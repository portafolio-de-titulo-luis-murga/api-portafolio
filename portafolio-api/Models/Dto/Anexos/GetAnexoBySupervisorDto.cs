using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using portafolio_api.Models.Dto.Provider;

namespace portafolio_api.Models.Dto.Anexos
{
    public class GetAnexoBySupervisorDto
    {
        public GetAnexoBySupervisorDto()
        {
            services = new List<GetServiceProvider>();
        }

        public int idAnexo { get; set; }
        public string facultyName { get; set; }
        public string unitName { get; set; }
        public string nameProvider { get; set; }
        public List<GetServiceProvider> services { get; set; }
    }
}