using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Anexos
{
    public class GetDstByAnexoDto
    {
        public int dst { get; set; }
        public int spokenSeconds { get; set; }
        public int numberOfAnsweredCalls { get; set; }
    }
}