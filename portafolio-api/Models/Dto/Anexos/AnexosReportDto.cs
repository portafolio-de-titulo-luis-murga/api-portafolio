using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Anexos
{
    public class AnexosReportDto
    {
        public int idAnexo { get; set; }
        public string unitName { get; set; }
        public string facultyName { get; set; }
        public string nameProvider { get; set; }
        public int? CELTotalSeg { get; set; }
        public int? CELTotalCost { get; set; }
        public int? CELCallQuantity { get; set; }
        public int? SLMTotalSeg { get; set; }
        public int? SLMTotalCost { get; set; }
        public int? SLMCallQuantity { get; set; }
        public int? LDITotalSeg { get; set; }
        public int? LDITotalCost { get; set; }
        public int? LDICallQuantity { get; set; }
        public int? totalSeg { get; set; } = 0;
        public int? totalCost { get; set; } = 0;
        public int? CallQuantity { get; set; } = 0;
    }
}