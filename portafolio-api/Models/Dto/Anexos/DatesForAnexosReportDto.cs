using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Anexos
{
    public class DatesForAnexosReportDto
    {
        public string initDate { get; set; }
        public string lastDate { get; set; }
    }
}