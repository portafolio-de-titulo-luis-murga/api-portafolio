using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Anexos
{
    public class DataAnexoService
    {
        public int idService { get; set; }
        public string nameService { get; set; } = "";
        public int spokenSeconds { get; set; }
        public int cost { get; set; }
    }
}