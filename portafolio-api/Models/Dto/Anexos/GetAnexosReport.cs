using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Anexos
{
    public class GetAnexosReport
    {
        public int idAnexo { get; set; }
        public string unitName { get; set; }
        public string facultyName { get; set; }
        public string nameProvider { get; set; }
    }
}