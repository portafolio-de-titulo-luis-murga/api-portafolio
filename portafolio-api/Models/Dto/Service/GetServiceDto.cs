using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Service
{
    public class GetServiceDto
    {
        public int idService { get; set; }
        public string nameService { get; set; } = null!;
    }
}