﻿namespace portafolio_api.Models.Dto.Auth
{
    public class LoginDto
    {
        public string username { get; set; }
        public string pwd { get; set; }
    }
}
