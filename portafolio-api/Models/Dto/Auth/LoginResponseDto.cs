﻿namespace portafolio_api.Models.Dto.Auth
{
    public class LoginResponseDto
    {
        public string token { get; set; }
        public string refreshToken { get; set; }
    }
}
