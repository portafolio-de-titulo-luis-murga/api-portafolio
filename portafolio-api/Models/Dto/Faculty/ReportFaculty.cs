using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Faculty
{
    public class ReportFaculty
    {
        public int idFaculty { get; set; }
        public string facultyName { get; set; }
        public int numberOfCall { get; set; }
        public double spokenSecond { get; set; }
    }
}