using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Faculty
{
    public class GetFacultyDto
    {
        public int idFaculty { get; set; }
        public string facultyName { get; set; } = "";
    }
}