using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Faculty
{
    public class ReportAllUniversityDto
    {
        public double _totalSpokenMin;
        public double totalSpokenMin
        {
            get
            {
                return _totalSpokenMin;
            }
            set
            {
                _totalSpokenMin = Math.Truncate(100 * value) / 100;
            }
        }
        public int totalCalls { get; set; } = 0;
        public List<ReportFaculty> facultiesReport { get; set; }

        public ReportAllUniversityDto()
        {
            facultiesReport = new List<ReportFaculty>();
        }

       

    }
}