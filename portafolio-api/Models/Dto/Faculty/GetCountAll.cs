using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Faculty
{
    public class GetCountAll
    {
        public int countSupervisor { get; set; }                 
        public int countProvider { get; set; }
        public int countFaculty { get; set; }
        public int countUnit { get; set; }
        public int countAnexo { get; set; }
        public int countStorageCall { get; set; }
        public int countNumberOfCallAnswered { get; set; }
        public int countSpokenSecond { get; set; }
        }
}