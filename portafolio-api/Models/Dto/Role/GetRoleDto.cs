using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Role
{
    public class GetRoleDto
    {
        public int idRole { get; set; }
        public string roleName { get; set; } = "";
        public string description { get; set; } = "";
    }
}