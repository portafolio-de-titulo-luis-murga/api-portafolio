using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Provider
{
    public class GetProviderDto
    {
        public GetProviderDto()
        {
            services = new List<GetServiceProvider>();
        }

        public int idProvider { get; set; }
        public string nameProvider { get; set; } = null!;
        public int fixedCost { get; set; }
        public int hasRelationFaculties { get; set; }
        public List<GetServiceProvider> services { get; set; }
    }
}