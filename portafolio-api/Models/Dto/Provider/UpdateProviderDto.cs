using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Provider
{
    public class UpdateProviderDto
    {
        public int idProvider { get; set; }
        public string nameProvider { get; set; } = "";
        public int fixedCost { get; set; }
        public int idService1 { get; set; }
        public int cost_service1 { get; set; }
        public int idService2 { get; set; }
        public int cost_service2 { get; set; }
        public int idService3 { get; set; }
        public int cost_service3 { get; set; }
    }
}
