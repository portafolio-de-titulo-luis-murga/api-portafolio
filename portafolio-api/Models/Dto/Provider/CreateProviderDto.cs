﻿namespace portafolio_api.Models.Dto.Provider
{
    public class CreateProviderDto
    {
        public string nameProvider { get; set; } = "";
        public int fixedCost { get; set; }

        public List<CostServiceCreateProvider> services { get; set; }

    }
}
