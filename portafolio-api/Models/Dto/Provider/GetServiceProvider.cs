using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Dto.Provider
{
    public class GetServiceProvider
    {
        public int idService { get; set; }
        public string nameService { get; set; }
        public int costService { get; set; }
    }
}