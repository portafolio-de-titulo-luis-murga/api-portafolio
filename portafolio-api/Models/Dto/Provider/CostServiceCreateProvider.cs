﻿namespace portafolio_api.Models.Dto.Provider
{
    public class CostServiceCreateProvider
    {
        public int idService { get; set; }
        public int costService { get; set; }
    }
}
