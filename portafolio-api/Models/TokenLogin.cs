using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models
{
    public class TokenLogin
    {
        public string token { get; set; }
        public string refreshToken { get; set; }
    }
}