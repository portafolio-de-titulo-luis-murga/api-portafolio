using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Data
{
    public class UserSessions
    {
        public string refreshToken { get; set; }
        public string jwtId { get; set; }
        public int idUser { get; set; }
    }
}