using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Data
{
    public class ResponseGeneratePdf
    {
        public string fileName { get; set; }
        public byte[] pdfFile { get; set; }
    }
}