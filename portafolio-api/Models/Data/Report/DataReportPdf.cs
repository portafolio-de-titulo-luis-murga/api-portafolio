using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Data
{
    public class DataReportPdf<T>
    {
        public string idTemplate { get; set; }
        public T data { get; set; }
    }
}