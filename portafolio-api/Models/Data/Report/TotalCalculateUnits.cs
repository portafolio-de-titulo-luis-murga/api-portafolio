using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models.Data.Report
{
    public class TotalCalculateUnits
    {
        public int? countUnits { get; set; } = 0;
        public int? totalSeg { get; set; } = 0; // Tiempo total hablado
        public int? totalCost { get; set; } = 0; // Total costo de todos los anexos
        public int? totalCallQuantity { get; set; } = 0; // Cantidad total de llamadas
        public int? SLMTotalSeg { get; set; } = 0;
        public int? SLMTotalCost { get; set; } = 0;
        public int? SLMTotalCall { get; set; } = 0;
        public int? CELTotalSeg { get; set; } = 0;
        public int? CELTotalCost { get; set; } = 0;
        public int? CELTotalCall { get; set; } = 0;
        public int? LDITotalSeg { get; set; } = 0;
        public int? LDITotalCost { get; set; } = 0;
        public int? LDITotalCall { get; set; } = 0;
    }
}