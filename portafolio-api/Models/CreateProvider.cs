﻿namespace portafolio_api.Models
{
    public class CreateProvider
    {
        public int idProvider { get; set; } = 0;
        public string nameProvider { get; set; } = "";
        public int fixedCost { get; set; }
    }
}
