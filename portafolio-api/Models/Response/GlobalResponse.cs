﻿namespace portafolio_api.Models.Response
{
    public class GlobalResponse<T>// esto significa que es de cualquier tipo(T)
    {
        public string code { get; set; } = string.Empty;
        public string message { get; set; } = string.Empty;
        public T? data { get; set; }
    }
}
