using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Models
{
    public class CrudUser
    {
        public int idUser { get; set; } = 0;
        public string username { get; set; }
        public string pwd { get; set; }
        public string email { get; set; }
        public string firstName { get; set; } = "";
        public string lastName { get; set; } = "";
        public int idRole { get; set; }
        public char action { get; set; }
    }
}