﻿namespace portafolio_api.Models
{
    public class UserRole
    {
        public int idUser { get; set; }
        public string userName { get; set; }
        public string pwdUser { get; set; }
        public string email { get; set; }
        public int idRole { get; set; }
        public string roleName { get; set; }
    }
}