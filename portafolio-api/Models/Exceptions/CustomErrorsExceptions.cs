﻿namespace portafolio_api.Models.Exceptions
{
    public class CustomErrorsExceptions : Exception //herencia o implementacion, es decir esta clase tambien es una clase de exepcion
    {
        public readonly string _code;
        public readonly int _httpCode;

        public CustomErrorsExceptions(string message, int httpCode, string code) : base(message)
        {
            _code = code;
            _httpCode = httpCode;
        }



    }
}
