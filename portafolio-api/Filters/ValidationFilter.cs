using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using portafolio_api.Models.Response;

namespace portafolio_api.Filters
{
    public class ValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if(!context.ModelState.IsValid)
            {
                GlobalResponse<object> response = new GlobalResponse<object>();
                response.code = "900";
                response.message = context.ModelState.Values.SelectMany(v => v.Errors).Select(m => m.ErrorMessage).First();

                context.Result = new BadRequestObjectResult(response);
                return;
            }

            await next();
        }
    }
}