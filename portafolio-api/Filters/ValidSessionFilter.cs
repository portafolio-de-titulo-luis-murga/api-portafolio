using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using portafolio_api.Models.Response;
using portafolio_api.Repositories;

namespace portafolio_api.Filters
{
    public class ValidSessionFilter : IAsyncActionFilter
    {
        private readonly UserSessionRepository _userSessionRepository;

        public ValidSessionFilter(UserSessionRepository userSessionRepository)
        {
            _userSessionRepository = userSessionRepository;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            GlobalResponse<string> response = new GlobalResponse<string>();
            response.code = "900";
            response.message = "Token no valido";

            // Buscamos el claim jti dentro de la autenticación del usuario
            var jti = context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti);

            if (jti == null)
            {
                context.Result = new UnauthorizedObjectResult(response);
                return;
            }

            // Validamos que aún tenga una sesión al Token enviado
            string existSession = await _userSessionRepository.validSessionByJwtId(jti.Value);

            if (!existSession.Equals("000")) 
            {
                context.Result = new UnauthorizedObjectResult(response);
                return;
            }

            await next();
        }
    }
}