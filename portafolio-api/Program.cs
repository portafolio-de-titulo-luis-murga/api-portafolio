
using AspNetCoreRateLimit;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using portafolio_api.Filters;
using portafolio_api.Models;
using portafolio_api.Models.Dto.User;
using portafolio_api.Repositories;
using portafolio_api.Services;
using portafolio_api.Utils;
using System.Data;
using System.Text;


var builder = WebApplication.CreateBuilder(args);

ConfigurationManager configuration = builder.Configuration;

#region Conexion base de datos
string connectString = configuration.GetConnectionString("Database");
builder.Services.AddTransient<IDbConnection>((sp) => new MySqlConnection(connectString));
#endregion

// Add services to the container.
#region Dependencias
builder.Services.AddScoped<ValidSessionFilter>();
builder.Services.AddScoped<AuthService>();
builder.Services.AddScoped<UserRepository>();
builder.Services.AddScoped<UserService>();
builder.Services.AddScoped<ServiceService>();
builder.Services.AddScoped<ServiceRepository>();
builder.Services.AddScoped<ProviderService>();
builder.Services.AddScoped<ProviderRepository>();
builder.Services.AddScoped<UnitService>();
builder.Services.AddScoped<UnitRepository>();
builder.Services.AddScoped<AnexoService>();
builder.Services.AddScoped<AnexoRepository>();
builder.Services.AddScoped<RoleService>();
builder.Services.AddScoped<RoleRepository>();
builder.Services.AddScoped<FacultyService>();
builder.Services.AddScoped<FacultyRepository>();
builder.Services.AddScoped<TokenService>();
builder.Services.AddScoped<UserSessionRepository>();
builder.Services.AddScoped<SendEmailService>();
builder.Services.AddScoped<StorageCallRepository>();
builder.Services.AddScoped<ReportRepository>();
builder.Services.AddScoped<ReportService>();
builder.Services.AddScoped<DateUtil>();
builder.Services.AddScoped<MethodsUtil>();
builder.Services.AddScoped<IPasswordHasher<UserRole>, PasswordHasher<UserRole>>();
builder.Services.AddScoped<IPasswordHasher<CreateUserDto>, PasswordHasher<CreateUserDto>>();
builder.Services.AddHttpContextAccessor();
#endregion

#region RateLimit
builder.Services.AddMemoryCache();
builder.Services.Configure < IpRateLimitOptions > (configuration.GetSection("IpRateLimiting"));
builder.Services.AddSingleton < IIpPolicyStore, MemoryCacheIpPolicyStore > ();
builder.Services.AddSingleton < IRateLimitCounterStore, MemoryCacheRateLimitCounterStore > ();
builder.Services.AddSingleton < IRateLimitConfiguration, RateLimitConfiguration > ();
builder.Services.AddSingleton < IProcessingStrategy, AsyncKeyLockProcessingStrategy > ();
builder.Services.AddInMemoryRateLimiting();
#endregion

#region Cors
builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
        builder =>
        {
            builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
        });
});
#endregion

#region Controllers
builder.Services.AddControllers(config =>
{
    config.ReturnHttpNotAcceptable = true;
    config.Filters.Add<ValidationFilter>();
}).AddXmlDataContractSerializerFormatters()
    .ConfigureApiBehaviorOptions(options =>
    {
        options.SuppressModelStateInvalidFilter = true;
    }).AddFluentValidation(options =>
    {
        options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
    });
#endregion
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region TOKEN JWT
var jwtSettings = configuration.GetSection("JwtSettings");

string secretKey = jwtSettings.GetValue<string>("SecretKey");

var key = Encoding.ASCII.GetBytes(secretKey);

var tokenValidationParams = new TokenValidationParameters
{
    ValidateIssuerSigningKey = true,
    IssuerSigningKey = new SymmetricSecurityKey(key),
    ValidateIssuer = false,
    ValidateAudience = false,
    ValidateLifetime = true,
    ClockSkew = TimeSpan.Zero
};

builder.Services.AddSingleton(tokenValidationParams);

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(jwt =>
{
    jwt.RequireHttpsMetadata = false;
    jwt.SaveToken = true;
    jwt.TokenValidationParameters = tokenValidationParams;
});
#endregion


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("CorsPolicy");
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.UseIpRateLimiting();

app.Run();
