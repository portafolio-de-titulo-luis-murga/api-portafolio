using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace portafolio_api.Utils
{
    public class DateUtil
    {
        public (string initDate, string lastDate) GetDatesOfMonthYear(string monthYear)
        {
            // Calcular fecha inicio y fecha fin
            var parts = monthYear.Split('/');
            int month = int.Parse(parts[0]);
            int year = int.Parse(parts[1]);
            // Creamos la fecha para el 27 del mes dado
            DateTime lastDate = new DateTime(year, month, 27);
            // Creamos la fecha para el 28 del mes anterior
            DateTime initDate = lastDate.AddMonths(-1).AddDays(1);
            return (initDate.ToString("yyyy-MM-dd"), lastDate.ToString("yyyy-MM-dd"));
        }
    }
}