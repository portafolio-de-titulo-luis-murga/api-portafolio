using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using portafolio_api.Models.Data;
using portafolio_api.Models.Exceptions;

namespace portafolio_api.Repositories
{
    public class ReportRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ReportRepository> _logger;
        private readonly HttpClient _httpClient;

        public ReportRepository(IConfiguration configuration, ILogger<ReportRepository> logger)
        {
            _configuration = configuration;
            _logger = logger;
            _httpClient = new HttpClient();
            var hostMSPdf = _configuration.GetSection("GeneraPdf");
            _httpClient.BaseAddress = new Uri(hostMSPdf.GetValue<string>("host"));
        }

        public async Task<ResponseGeneratePdf> GeneratePdf(DataReportPdf<Dictionary<string, JsonElement>> genericObj)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync("/ms/v1/pdf/generate", genericObj);

                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadFromJsonAsync<ResponseGeneratePdf>();
                    _logger.LogInformation("[GeneratePdf] Generacion de PDF Ok!");
                    return result;
                }

                _logger.LogError("[GeneratePdf] Llamado a Generacion de PDF responde con error");
                throw new CustomErrorsExceptions("Error al Generar PDF", 500, "900");
            }
            catch (Exception ex)
            {
                _logger.LogError("[GeneratePdf] Error al Generar PDF: ", ex.Message);
                throw;
            }
        }
    }
}