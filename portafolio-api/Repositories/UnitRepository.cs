using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using portafolio_api.Models.Data.Report;
using portafolio_api.Models.Dto.Unit;
using portafolio_api.Models.Exceptions;

namespace portafolio_api.Repositories
{
    public class UnitRepository
    {
        private readonly IDbConnection _dbConnection;

        private readonly ILogger<UnitRepository> _logger;

        public UnitRepository(IDbConnection dbConnection, ILogger<UnitRepository> logger)
        {
            _dbConnection = dbConnection;
            _logger = logger;
        }
        public async Task<List<GetUnitDto>> getAll()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idUnit", value: 0, DbType.Int32, direction: ParameterDirection.Input);

                var unitDictionary = new Dictionary<int, GetUnitDto>();

                var resp = await _dbConnection.QueryAsync<GetUnitDto, GetAnexoUnit, GetUnitDto>(
                    sql: "SP_GET_UNIT",
                    (unit, anexos) => {
                        GetUnitDto unitEntry;
                        if (!unitDictionary.TryGetValue(unit.idUnit, out unitEntry))
                        {
                            unitEntry = unit;
                            unitDictionary.Add(unitEntry.idUnit, unitEntry);
                        }
                        if (anexos != null && anexos.idAnexo > 0)
                        {
                            unitEntry.numberOfAnexo +=1;
                            unitEntry.anexos.Add(anexos);
                        }

                        return unitEntry;
                    },
                    splitOn: "idAnexo",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo todas las unidades", ex.Message);
                throw;
            }
        }

        public async Task<GetUnitDto?> getUnitId(int idUnit)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idUnit", value: idUnit, DbType.Int32, direction: ParameterDirection.Input);

                var resp = await _dbConnection.QueryAsync<GetUnitDto>(
                    sql: "SP_GET_UNIT",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                if (resp.Any())
                {
                    return resp.First();
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError("[getUnitId] Error obteniendo la unidad ", ex.Message);
                throw;
            }
        }

        public async Task<string> create(CreateUpdateUnitDto createUpdateUnitDto)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_unitName", value: createUpdateUnitDto.unitName, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idFaculty", value: createUpdateUnitDto.idFaculty, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idUser", value: createUpdateUnitDto.idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_CREATE_UNIT",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_CREATE_UNIT", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[create] Error creando unidad: ", ex.Message);
                throw;
            }
        }

        public async Task<string> update(CreateUpdateUnitDto createUpdateUnitDto, int idUnit)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idUnit", value: idUnit, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_unitName", value: createUpdateUnitDto.unitName, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idFaculty", value: createUpdateUnitDto.idFaculty, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idUser", value: createUpdateUnitDto.idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_UPDATE_UNIT",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_UPDATE_UNIT", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[update] Error actualizando unidad: ", ex.Message);
                throw;
            }
        }

        public async Task<string> delete(int idUnit)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idUnit", value: idUnit, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_UNIT_DELETE",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_UNIT_DELETE", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[delete] Error eliminando unidad: ", ex.Message);
                throw;
            }
        }

        public async Task<object?> getUnitReport(string initDate, string lastDate, int idUnit, bool individual = false)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idUnit", value: idUnit, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaInicio", value: initDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaFin", value: lastDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);
        
                var unitDictionary = new Dictionary<int, UnitsReportDto>();

                var resp = await _dbConnection.QueryAsync<GetUnitReport, GetUnitServiceReport, UnitsReportDto>(
                    sql: "SP_ALL_UNIT",
                    (unit, service) => {
                        UnitsReportDto unitEntry;
                        if (!unitDictionary.TryGetValue(unit.idUnit, out unitEntry))
                        {
                            unitEntry = new UnitsReportDto
                            {
                              idUnit = unit.idUnit,
                              unitName = unit.unitName,
                              userName = unit.userName,
                              facultyName = unit.facultyName,
                              nameProvider = unit.nameProvider,
                              countAnexos = unit.countAnexos,
                              fixedCostAnexos = unit.fixedCostAnexos
                            };

                            unitDictionary.Add(unitEntry.idUnit, unitEntry);
                        }

                        if (service.nameService.Equals("CEL")) 
                        {
                          unitEntry.CELTotalCost = service.cost;
                          unitEntry.CELTotalSeg = service.spokenSeconds;
                          unitEntry.CELCallQuantity = service.numberOfAnsweredCalls;
                          unitEntry.CELCostService = service.costService;
                        }
                        else if (service.nameService.Equals("SLM"))
                        {
                          unitEntry.SLMTotalCost = service.cost;
                          unitEntry.SLMTotalSeg = service.spokenSeconds;
                          unitEntry.SLMCallQuantity = service.numberOfAnsweredCalls;
                          unitEntry.SLMCostService = service.costService;
                        }
                        else if (service.nameService.Equals("LDI"))
                        {
                          unitEntry.LDITotalCost = service.cost;
                          unitEntry.LDITotalSeg = service.spokenSeconds;
                          unitEntry.LDICallQuantity = service.numberOfAnsweredCalls;
                          unitEntry.LDICostService = service.costService;
                        }

                        unitEntry.totalSeg += service.spokenSeconds;
                        unitEntry.totalCost += service.cost;
                        unitEntry.CallQuantity += service.numberOfAnsweredCalls;
                        return unitEntry;
                    },
                    splitOn: "idService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                if (individual) {
                  if (resp.Any())
                  {
                    return resp.Distinct().First();
                  }
                  return null;
                }
                return resp.Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getUnitReport] Error obteniendo reporte unidades ", ex.Message);
                throw;
            }
        }

        public async Task<TotalCalculateUnits> calcTotalTarification(string initDate, string lastDate)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idUnit", value: 0, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaInicio", value: initDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaFin", value: lastDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var countUnitDictionary = new Dictionary<int, int>();

                TotalCalculateUnits totalCalculateUnits = new TotalCalculateUnits();

                await _dbConnection.QueryAsync<GetUnitReport, GetUnitServiceReport, TotalCalculateUnits>(
                    sql: "SP_ALL_UNIT",
                    (unit, service) => {
                        int countUnits = 0;
                        if (!countUnitDictionary.TryGetValue(unit.idUnit, out countUnits))
                        {
                            totalCalculateUnits.countUnits += 1;
                            countUnits += 1;
                            countUnitDictionary.Add(unit.idUnit, countUnits);
                        }

                        if (service.nameService.Equals("CEL")) 
                        {
                          totalCalculateUnits.CELTotalSeg += service.spokenSeconds;
                          totalCalculateUnits.CELTotalCost += service.cost;
                          totalCalculateUnits.CELTotalCall += service.numberOfAnsweredCalls;
                        }
                        else if (service.nameService.Equals("SLM"))
                        {
                          totalCalculateUnits.SLMTotalSeg += service.spokenSeconds;
                          totalCalculateUnits.SLMTotalCost += service.cost;
                          totalCalculateUnits.SLMTotalCall += service.numberOfAnsweredCalls;
                        }
                        else if (service.nameService.Equals("LDI"))
                        {
                          totalCalculateUnits.LDITotalSeg += service.spokenSeconds;
                          totalCalculateUnits.LDITotalCost += service.cost;
                          totalCalculateUnits.LDITotalCall += service.numberOfAnsweredCalls;
                        }

                        totalCalculateUnits.totalSeg += service.spokenSeconds;
                        totalCalculateUnits.totalCost += service.cost;
                        totalCalculateUnits.totalCallQuantity += service.numberOfAnsweredCalls;
                        return totalCalculateUnits;
                    },
                    splitOn: "idService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return totalCalculateUnits;
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo tarificacion unidades: ", ex.Message);
                throw;
            }
        }

    }
}