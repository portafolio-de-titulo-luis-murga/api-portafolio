using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using portafolio_api.Models.Data;
using portafolio_api.Models.Exceptions;

namespace portafolio_api.Repositories
{
    public class UserSessionRepository
    {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<ServiceRepository> _logger;

        public UserSessionRepository(IDbConnection dbConnection, ILogger<ServiceRepository> logger)
        {
          _dbConnection = dbConnection;
          _logger = logger;
        }

        public async Task<string> validSessionByJwtId(string jwtId)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_jwt_id", value: jwtId, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "out_code", direction: ParameterDirection.Output);

                string nameSP = "SP_VALID_SESSION_BY_JWTID";

                await _dbConnection.QueryAsync(
                    sql: nameSP,
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string responseSP = dynamicParameters.Get<string>("out_code");

                if (responseSP.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al validar sesion", 500, "900");
                }
                return responseSP;
            }
            catch (Exception ex)
            {
                _logger.LogError("[validSessionByJwtId] Error al validar sesion", ex.Message);
                throw;
            }
        }

        public async void findTokenAndDelete(int userId)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_user_id", value: userId, dbType: DbType.String, direction: ParameterDirection.Input);

                string nameSP = "SP_VALID_SESSION_AND_DELETE";

                await _dbConnection.ExecuteAsync(
                    sql: nameSP,
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );
            }
            catch (Exception ex)
            {
                _logger.LogError("[findTokenAndDelete] Error al buscar token", ex.Message);
                throw;
            }
        }

        public async void createSessionToken(UserSessions userSessions)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_jwt_id", value: userSessions.jwtId, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_token", value: userSessions.refreshToken, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_user_id", value: userSessions.idUser, dbType: DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "out_code", direction: ParameterDirection.Output);

                string nameSP = "SP_CREATE_SESSION_TOKEN";

                await _dbConnection.QueryAsync(
                    sql: nameSP,
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string responseSP = dynamicParameters.Get<string>("out_code");

                if (!responseSP.Equals("000"))
                {
                    throw new CustomErrorsExceptions("[createSessionToken] Error al crear sesion", 500, "900");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}