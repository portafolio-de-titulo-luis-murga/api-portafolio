using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using portafolio_api.Models.Dto.Role;

namespace portafolio_api.Repositories
{
    public class RoleRepository
    {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<RoleRepository> _logger;

        public RoleRepository(IDbConnection dbConnection, ILogger<RoleRepository> logger)
        {
            _dbConnection = dbConnection;
            _logger = logger;
        }
        public async Task<List<GetRoleDto>> getAll()
        {
          try
          {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add(name: "p_idRole", value: 0, DbType.Int32, direction: ParameterDirection.Input);

            var resp = await _dbConnection.QueryAsync<GetRoleDto>(
            sql: "SP_GET_ROLE",
            param: dynamicParameters,
            commandType: CommandType.StoredProcedure
          );

          return resp.ToList();
          }
          catch (Exception ex)
          {
            _logger.LogError("[getAll] Error obteniendo todos los roles", ex.Message);
            throw;
          }
        }


        public async Task<GetRoleDto?> getById(int idRole)
        {
          try
          {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add(name: "p_idRole", value: idRole, DbType.Int32, direction: ParameterDirection.Input);

            var resp = await _dbConnection.QueryAsync<GetRoleDto>(
              sql: "SP_GET_ROLE",
              param: dynamicParameters,
              commandType: CommandType.StoredProcedure
            );

            if (resp.Any())//viene algo o no viene algo en este listado de datos
            {
                return resp.First();
            }

            return null;
          }
          catch (Exception ex)
          {
            _logger.LogError("[getById] Error obteniendo el role", ex.Message);
            throw;
          }
        }
    }
}