using System.Data;
using Dapper;
using portafolio_api.Models.Dto.Service;

namespace portafolio_api.Repositories
{
  public class ServiceRepository
  {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<ServiceRepository> _logger;

        public ServiceRepository(IDbConnection dbConnection, ILogger<ServiceRepository> logger)
        {
          _dbConnection = dbConnection;
          _logger = logger;
        }

        public async Task<List<GetServiceDto>> getAll()
        {
          try
          {
            IEnumerable<GetServiceDto> resp = await _dbConnection.QueryAsync<GetServiceDto>(
            sql: "SP_GET_SERVICES",
            commandType: CommandType.StoredProcedure
          );

          return resp.ToList();
          }
          catch (Exception ex)
          {
            _logger.LogError("[getAll] Error obteniendo todos los Servicios", ex.Message);
            throw;
          }

        }
  }
}