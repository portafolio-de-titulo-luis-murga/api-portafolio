﻿using Dapper;
using portafolio_api.Models;
using portafolio_api.Models.Dto.Provider;
using portafolio_api.Models.Exceptions;
using System.Configuration;
using System.Data;

namespace portafolio_api.Repositories
{
    public class ProviderRepository
    {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<ProviderRepository> _logger;

        public ProviderRepository(IDbConnection dbConnection, ILogger<ProviderRepository> logger)
        {
            _dbConnection = dbConnection;
            _logger = logger;
        }

        //Primer metodo que llama al SP Provedores
        public async Task<ResponseCreateProvider> createProvider(CreateProvider providerCreate)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idProvider", value: providerCreate.idProvider, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_nameProvider", value: providerCreate.nameProvider, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fixedCost", value: providerCreate.fixedCost, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);
                dynamicParameters.Add(name: "out_idProvider", direction: ParameterDirection.Output);

                await _dbConnection.QueryAsync(
                    sql: "SP_CREATE_PROVIDER",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_CREATE_PROVIDER", 500, "900");
                }

                ResponseCreateProvider resp = new ResponseCreateProvider()
                {
                    codeRespSP = outputResp
                };

                if (outputResp.Equals("000")) {
                    int outIdProvider = dynamicParameters.Get<int>("out_idProvider");
                    resp.idProvider = outIdProvider;
                }

                return resp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        //Segundo metodo que llama al SP CostService
        public async Task<string> crudCostService(CrudCostService costServiceCrud)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_action", value: costServiceCrud.action, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idService", value: costServiceCrud.idService, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_costService", value: costServiceCrud.costService, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idProvider", value: costServiceCrud.idProvider, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                sql: "SP_CRUD_COST_SERVICE_PROVIDER",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_CRUD_PROVIDER", 500, "900");
                }

                return outputResp;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public async Task<List<GetProviderDto>> getAll()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idProvider", value: 0, DbType.String, direction: ParameterDirection.Input);

                var providerDictionary = new Dictionary<int, GetProviderDto>();

                var resp = await _dbConnection.QueryAsync<GetProviderDto, GetServiceProvider, GetProviderDto>(
                    sql: "SP_GET_PROVIDER",
                    (provider, services) => {
                        GetProviderDto providerEntry;
                        if (!providerDictionary.TryGetValue(provider.idProvider, out providerEntry))
                        {
                            providerEntry = provider;
                            providerDictionary.Add(providerEntry.idProvider, providerEntry);
                        }

                        if (services.idService > 0)
                        {
                            providerEntry.services.Add(services);
                        }
                        return providerEntry;
                    },
                    splitOn: "idService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo todos los Proveedores", ex.Message);
                throw;
            }
        }


        public async Task<GetProviderDto?> getProviderId(int idProvider)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idProvider", value: idProvider, DbType.Int32, direction: ParameterDirection.Input);

                var providerDictionary = new Dictionary<int, GetProviderDto>();

                var resp = await _dbConnection.QueryAsync<GetProviderDto, GetServiceProvider, GetProviderDto>(
                    sql: "SP_GET_PROVIDER",
                    (provider, services) => {
                        GetProviderDto providerEntry;
                        if (!providerDictionary.TryGetValue(provider.idProvider, out providerEntry))
                        {
                            providerEntry = provider;
                            providerDictionary.Add(providerEntry.idProvider, providerEntry);
                        }

                        if (services.idService > 0)
                        {
                            providerEntry.services.Add(services);
                        }
                        return providerEntry;
                    },
                    splitOn: "idService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                if (resp.Any())
                {
                    return resp.Distinct().First();
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError("[getProviderId] Error obteniendo el provedor ", ex.Message);
                throw;
            }
        }


        public async Task<string> deleteProvider(int idProvider)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idProvider", value: idProvider, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                sql: "SP_PROVIDER_DELETE",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_CRUD_PROVIDER", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[deleteProvider] Error eliminando el provedor ", ex.Message);
                throw;
            }
        }

        public async Task<string> deleteProviderWithFaculty(int idProvider, int idNewProvider)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idProvider", value: idProvider, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idNewProvider", value: idNewProvider, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                sql: "SP_PROVIDER_DELETE_WITH_FACULTY",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_PROVIDER_DELETE_WITH_FACULTY", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[deleteProviderWithFaculty] Error eliminando el provedor con facultad asociada ", ex.Message);
                throw;
            }
        }

        public async Task<string> updateProvider(UpdateProviderDto updateProviderDto)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idProvider", value: updateProviderDto.idProvider, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_nameProvider", value: updateProviderDto.nameProvider, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fixedCost", value: updateProviderDto.fixedCost, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_id_service1", value: updateProviderDto.idService1, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_cost_service1", value: updateProviderDto.cost_service1, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_id_service2", value: updateProviderDto.idService2, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_cost_service2", value: updateProviderDto.cost_service2, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_id_service3", value: updateProviderDto.idService3, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_cost_service3", value: updateProviderDto.cost_service3, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                sql: "SP_UPDATE_PROVIDER",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_UPDATE_PROVIDER", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[updateProvider] Error actualizando el provedor", ex.Message);
                throw;
            }
        }

    }
}
