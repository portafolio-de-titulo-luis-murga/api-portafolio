﻿using Dapper;
using portafolio_api.Models;
using portafolio_api.Models.Dto.Anexos;
using portafolio_api.Models.Dto.Unit;
using portafolio_api.Models.Dto.User;
using portafolio_api.Models.Exceptions;
using System.Data;

namespace portafolio_api.Repositories
{
    public class UserRepository
    {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<UserRepository> _logger;

        public UserRepository(IDbConnection dbConnection, ILogger<UserRepository> logger) 
        {
            _dbConnection = dbConnection;
            _logger = logger;
        }

        //metodo de tipo string que lleva TASK porque es asincrono, porque debe llevar una tarea
        public async Task<UserRole?> getUserByUsername(string username)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "nameUser", value: username, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                //el await queda esperando hasta que se resuelve el llamado a la bd.
                IEnumerable<UserRole> spResponse = await _dbConnection.QueryAsync<UserRole>(
                    sql: "SP_VERIFY_USER",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900")) {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_VERIFY_USER", 500, "900");
                }

                if (spResponse.Any())//viene algo o no viene algo en este listado de datos
                {
                    UserRole userRole = spResponse.First();
                    return userRole;
                }

                return null;

            }
            catch (Exception)
            {
                //Agregar loggs
                throw;
            }
        }

        public async Task<string> crudUser(CrudUser userCrud)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_action", value: userCrud.action, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idUser", value: userCrud.idUser, dbType: DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idRole", value: userCrud.idRole, dbType: DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_username", value: userCrud.username, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_pwd", value: userCrud.pwd, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_email", value: userCrud.email, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_firstName", value: userCrud.firstName, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_lastName", value: userCrud.lastName, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_CRUD_USER",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900")) {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_CRUD_USER", 500, "900");
                }
                return outputResp;
            }
            catch (Exception)
            {
                //Agregar loggs
                throw;
            }
        }

        public async Task<List<GetUserDto>> getAll()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idUser", value: 0, DbType.Int32, direction: ParameterDirection.Input);

                var resp = await _dbConnection.QueryAsync<GetUserDto>(
                sql: "SP_GET_USER",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                return resp.Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo todos los usuarios", ex.Message);
                throw;
            }
        }

        public async Task<GetUserDto?> getById(int idUser)
        {
          try
          {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);

            var resp = await _dbConnection.QueryAsync<GetUserDto>(
              sql: "SP_GET_USER",
              param: dynamicParameters,
              commandType: CommandType.StoredProcedure
            );

            if (resp.Any())//viene algo o no viene algo en este listado de datos
            {
                return resp.First();
            }

            return null;
          }
          catch (Exception ex)
          {
            _logger.LogError("[getById] Error obteniendo el usuario", ex.Message);
            throw;
          }
        }
    
        public async Task<UserDataReport?> getPdfReport(string initDate, string lastDate, int idUser)
        {
          try
          {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
            dynamicParameters.Add(name: "p_fechaInicio", value: initDate, DbType.String, direction: ParameterDirection.Input);
            dynamicParameters.Add(name: "p_fechaFin", value: lastDate, DbType.String, direction: ParameterDirection.Input);
            dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

            var userDictionary = new Dictionary<int, UserDataReport>();

            var resp = await _dbConnection.QueryAsync<UserDataReport, GetUnitServiceReport, UserDataReport>(
                sql: "SP_PDF_RTRU",
                (user, service) => {
                    UserDataReport userDataReportEntry;
                    if (!userDictionary.TryGetValue(user.idUser, out userDataReportEntry))
                    {
                        userDataReportEntry = new UserDataReport
                        {
                            idUser = user.idUser,
                            userName = user.userName,
                            cantUnidades = user.cantUnidades,
                            cantAnexos = user.cantAnexos,
                            countCall = user.countCall,
                            totalCallTime = user.totalCallTime,
                            totalCost = user.totalCost
                        };
                        userDictionary.Add(user.idUser, userDataReportEntry);
                    }

                    userDataReportEntry.services.Add(service);

                    userDataReportEntry.countCall += service.numberOfAnsweredCalls;
                    userDataReportEntry.totalCallTime += service.spokenSeconds;
                    userDataReportEntry.totalCost += service.cost;
                    return userDataReportEntry;
                },
                splitOn: "idService",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
            );

            if (resp.Any())
            {
                return resp.Distinct().First();
            }

            return null;
          }
          catch (Exception ex)
          {
            _logger.LogError("[getPdfReport] Error obteniendo data reporte supervisor: ", ex.Message);
            throw;
          }
        }

        public async Task<List<UserReportAnexoExcel>> getExcelReport(string initDate, string lastDate, int idUser)
        {
          try
          {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
            dynamicParameters.Add(name: "p_fechaInicio", value: initDate, DbType.String, direction: ParameterDirection.Input);
            dynamicParameters.Add(name: "p_fechaFin", value: lastDate, DbType.String, direction: ParameterDirection.Input);
            dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

            var userDictionary = new Dictionary<int, UserReportAnexoExcel>();

            var resp = await _dbConnection.QueryAsync<UserReportAnexoExcel, GetAnexosServiceReport, UserReportAnexoExcel>(
                sql: "SP_EXCEL_RTRU",
                (user, service) => {
                    UserReportAnexoExcel userDataReportEntry;
                    if (!userDictionary.TryGetValue(user.idAnexo, out userDataReportEntry))
                    {
                        userDataReportEntry = new UserReportAnexoExcel
                        {
                            userName = user.userName,
                            unitName = user.unitName,
                            idAnexo = user.idAnexo
                        };

                        userDictionary.Add(user.idAnexo, userDataReportEntry);
                    }

                    if (service.nameService.Equals("CEL")) 
                    {
                        userDataReportEntry.celTotalSeg = service.spokenSeconds;
                        userDataReportEntry.celTotalCost = service.cost;
                        userDataReportEntry.celTotalCall = service.numberOfAnsweredCalls;
                    }
                    else if (service.nameService.Equals("SLM"))
                    {
                        userDataReportEntry.slmTotalSeg = service.spokenSeconds;
                        userDataReportEntry.slmTotalCost = service.cost;
                        userDataReportEntry.slmTotalCall = service.numberOfAnsweredCalls;
                    }
                    else if (service.nameService.Equals("LDI"))
                    {
                        userDataReportEntry.ldiTotalSeg = service.spokenSeconds;
                        userDataReportEntry.ldiTotalCost = service.cost;
                        userDataReportEntry.ldiTotalCall = service.numberOfAnsweredCalls;
                    }

                    return userDataReportEntry;
                },
                splitOn: "nameService",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
            );

            return resp.Distinct().ToList();
          }
          catch (Exception ex)
          {
            _logger.LogError("[getExcelReport] Error obteniendo data reporte supervisor: ", ex.Message);
            throw;
          }
        }

        public async Task<List<GetResponsableDto>> getAllResponsable()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                //dynamicParameters.Add(name: "p_idUser", value: 0, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var resp = await _dbConnection.QueryAsync<GetResponsableDto>(
                sql: "SP_GET_RESPONSABLES",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                return resp.ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAllResponsable] Error obteniendo todos los responsabllees", ex.Message);
                throw;
            }
        }

        public async Task<EmailUser> resetPW(int idUser, string resetPw)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_resetPw", value: resetPw, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var resp = await _dbConnection.QueryAsync<EmailUser>(
                    sql: "SP_RESET_PW",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_RESET_PW", 500, "900");
                }else if (outputResp.Equals("001"))
                {
                     throw new CustomErrorsExceptions("Usuario no existe en los registros", 200, "001");
                }

                return resp.First();
            }
            catch (Exception ex)
            {
                _logger.LogError("[resetPW] Error reseteando contraseña", ex.Message);
                throw;
            }
        }

        public async Task<string> deleteUser(int idUser)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                sql: "SP_USER_DELETE",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_USER_DELETE", 500, "900");

                }else if (outputResp.Equals("001"))
                {
                     throw new CustomErrorsExceptions("Usuario no existe en los registros", 200, "001");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[deleteUser] Error eliminando el usuario ", ex.Message);
                throw;
            }
        }

        public async Task<string> deleteUserWithUnit(int idUser, int idNewUser)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idNewUser", value: idNewUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                sql: "SP_USER_DELETE_WITH_UNIT",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar deleteUserWithUnit", 500, "900");

                }else if (outputResp.Equals("001"))
                {
                     throw new CustomErrorsExceptions("Usuario no existe en los registros", 200, "001");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[deleteUserWithUnit] Error eliminando el usuario con unidad asociada ", ex.Message);
                throw;
            }
        }

        public async Task<string> updateUser(UpdateUser updateUser, int idUser)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_firstName", value: updateUser.firstName, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_lastName", value: updateUser.lastName, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_email", value: updateUser.email, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                sql: "SP_UPDATE_USER",
                param: dynamicParameters,
                commandType: CommandType.StoredProcedure
                );

                string resp = dynamicParameters.Get<string>("outCode");

                if (resp.Equals("900"))
                {//validacion del error en caso que venga desde la base de datos
                    throw new CustomErrorsExceptions("Error al ejecutar SP_UPDATE_USER", 500, "900");

                }else if (resp.Equals("001"))
                {
                     throw new CustomErrorsExceptions("Usuario no existe en los registros", 200, "001");
                }

                return resp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[updateUser] Error actualizando el usuario", ex.Message);
                throw;
            }
        }

        public async Task<string> updatePwd(int idUser, string newPwd)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                
                dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_newPwd", value: newPwd, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_CHANGE_PWD",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string resp = dynamicParameters.Get<string>("outCode");

                if (resp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_CHANGE_PWD", 500, "900");

                }

                return resp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[updatePwd] Error actualizando contraseña: ", ex.Message);
                throw;
            }
        }
    }
}
