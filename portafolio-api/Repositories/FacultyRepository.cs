using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using portafolio_api.Models.Dto.Faculty;

namespace portafolio_api.Repositories
{
    public class FacultyRepository
    {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<FacultyRepository> _logger;

        public FacultyRepository(IDbConnection dbConnection, ILogger<FacultyRepository> logger)
        {
            _dbConnection = dbConnection;
            _logger = logger;
        }

        public async Task<List<GetFacultyDto>> getAll()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idFaculty", value: 0, DbType.Int32, direction: ParameterDirection.Input);

                var resp = await _dbConnection.QueryAsync<GetFacultyDto>(
                    sql: "SP_GET_FACULTY",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo todas las facultades", ex.Message);
                throw;
            }
        }


        public async Task<GetFacultyDto?> getById(int idFaculty)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idFaculty", value: idFaculty, DbType.Int32, direction: ParameterDirection.Input);

                var resp = await _dbConnection.QueryAsync<GetFacultyDto>(
                    sql: "SP_GET_FACULTY",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                if (resp.Any())//viene algo o no viene algo en este listado de datos
                {
                    return resp.First();
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError("[getById] Error obteniendo la facultad", ex.Message);
                throw;
            }
        }

        public async Task<ReportAllUniversityDto> getReportAllUniversity(string initDate, string lastDate)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_fechaInicio", value: initDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaFin", value: lastDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                ReportAllUniversityDto reportAllUniversity = new ReportAllUniversityDto();

                var resp = await _dbConnection.QueryAsync<ReportFaculty, ReportFaculty, ReportAllUniversityDto>(
                    sql: "SP_ALL_UNIVERSITY",
                    (faculty1, faculty2) => {
                        reportAllUniversity.totalSpokenMin += faculty2.spokenSecond/60;
                        reportAllUniversity.totalCalls += faculty2.numberOfCall;
                        faculty2.idFaculty = faculty1.idFaculty;
                        faculty2.spokenSecond = Math.Truncate(100 * (faculty2.spokenSecond/60)) / 100;;
                        reportAllUniversity.facultiesReport.Add(faculty2);
                        return reportAllUniversity;
                    },
                    splitOn: "facultyName",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.First();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getReportAllUniversity] Error obteniendo reporte de universidad: ", ex.Message);
                throw;
            }
        }


        public async Task<GetCountAll> getAllCount()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var resp = await _dbConnection.QueryAsync<GetCountAll>(
                    sql: "SP_COUNT_HOME",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.First();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAllCount] Error obteniendo todas contadores", ex.Message);
                throw;
            }
        }
    }
}