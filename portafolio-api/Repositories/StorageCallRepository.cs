using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using portafolio_api.Models.Exceptions;

namespace portafolio_api.Repositories
{
    public class StorageCallRepository
    {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<StorageCallRepository> _logger;

        public StorageCallRepository(IDbConnection dbConnection, ILogger<StorageCallRepository> logger)
        {
            _dbConnection = dbConnection;
            _logger = logger;
        }

        public async Task<string> insertStorageCall()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_INSERT_STORAGECALL",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_INSERT_STORAGECALL", 500, "900");
                }

                return outputResp;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}