using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using portafolio_api.Models.Data.Report;
using portafolio_api.Models.Dto.Anexos;
using portafolio_api.Models.Dto.Provider;
using portafolio_api.Models.Exceptions;

namespace portafolio_api.Repositories
{
    public class AnexoRepository
    {
        private readonly IDbConnection _dbConnection;

        private readonly ILogger<AnexoRepository> _logger;

        public AnexoRepository(
            IDbConnection dbConnection, 
            ILogger<AnexoRepository> logger
        )
        {
            _dbConnection = dbConnection;
            _logger = logger;
        }

        public async Task<List<GetAnexoDto>> getAll()
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idAnexo", value: 0, DbType.Int32, direction: ParameterDirection.Input);
        
                IEnumerable<GetAnexoDto> resp = await _dbConnection.QueryAsync<GetAnexoDto>(
                    sql: "SP_GET_ANEXOS",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo todos los anexos", ex.Message);
                throw;
            }
        }

        public async Task<GetAnexoDto?> getById(int idAnexo)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idAnexo", value: idAnexo, DbType.Int32, direction: ParameterDirection.Input);

                var resp = await _dbConnection.QueryAsync<GetAnexoDto>(
                    sql: "SP_GET_ANEXOS",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                if (resp.Any())
                {
                    return resp.First();
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError("[getById] Error obteniendo anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<string> create(CreateUpdateAnexoDto createUpdateAnexoDto)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idAnexo", value: createUpdateAnexoDto.idAnexo, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idUnit", value: createUpdateAnexoDto.idUnit, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_CREATE_ANEXO",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_CREATE_ANEXO", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[create] Error creando anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<string> update(CreateUpdateAnexoDto createUpdateAnexoDto)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idAnexo", value: createUpdateAnexoDto.idAnexo, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idUnit", value: createUpdateAnexoDto.idUnit, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_UPDATE_ANEXO",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_UPDATE_ANEXO", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[update] Error actualizando anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<string> updateStatus(int idAnexo)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idAnexo", value: idAnexo, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                await _dbConnection.QueryAsync(
                    sql: "SP_STATUS_CHANGE_ANEXO",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                string outputResp = dynamicParameters.Get<string>("outCode");

                if (outputResp.Equals("900"))
                {
                    throw new CustomErrorsExceptions("Error al ejecutar SP_STATUS_CHANGE_ANEXO", 500, "900");
                }

                return outputResp;
            }
            catch (Exception ex)
            {
                _logger.LogError("[update] Error creando anexo: ", ex.Message);
                throw;
            }
        }

        public async Task<object?> getAllReport(string initDate, string lastDate, int idAnexo, bool individual = false)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idAnexo", value: idAnexo, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaInicio", value: initDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaFin", value: lastDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var anexoDictionary = new Dictionary<int, AnexosReportDto>();

                var resp = await _dbConnection.QueryAsync<GetAnexosReport, GetAnexosServiceReport, AnexosReportDto>(
                    sql: "SP_ALL_ANEXO",
                    (anexo, service) => {
                        AnexosReportDto anexoEntry;
                        if (!anexoDictionary.TryGetValue(anexo.idAnexo, out anexoEntry))
                        {
                            anexoEntry = new AnexosReportDto
                            {
                              idAnexo = anexo.idAnexo,
                              unitName = anexo.unitName,
                              facultyName = anexo.facultyName,
                              nameProvider = anexo.nameProvider
                            };

                            anexoDictionary.Add(anexoEntry.idAnexo, anexoEntry);
                        }

                        if (service.nameService.Equals("CEL")) 
                        {
                          anexoEntry.CELTotalCost = service.cost;
                          anexoEntry.CELTotalSeg = service.spokenSeconds;
                          anexoEntry.CELCallQuantity = service.numberOfAnsweredCalls;
                        }
                        else if (service.nameService.Equals("SLM"))
                        {
                          anexoEntry.SLMTotalCost = service.cost;
                          anexoEntry.SLMTotalSeg = service.spokenSeconds;
                          anexoEntry.SLMCallQuantity = service.numberOfAnsweredCalls;
                        }
                        else if (service.nameService.Equals("LDI"))
                        {
                          anexoEntry.LDITotalCost = service.cost;
                          anexoEntry.LDITotalSeg = service.spokenSeconds;
                          anexoEntry.LDICallQuantity = service.numberOfAnsweredCalls;
                        }

                        anexoEntry.totalSeg += service.spokenSeconds;
                        anexoEntry.totalCost += service.cost;
                        anexoEntry.CallQuantity += service.numberOfAnsweredCalls;
                        return anexoEntry;
                    },
                    splitOn: "idService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                if (individual) {
                  if (resp.Any())
                  {
                    return resp.Distinct().First();
                  }
                  return null;
                }
                return resp.Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo todos los Proveedores", ex.Message);
                throw;
            }
        }

        public async Task<TotalCalculateAnexos> calcTotalTarification(string initDate, string lastDate)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();

                dynamicParameters.Add(name: "p_idAnexo", value: 0, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaInicio", value: initDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_fechaFin", value: lastDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var countAnexoDictionary = new Dictionary<int, int>();

                TotalCalculateAnexos totalCalculateAnexos = new TotalCalculateAnexos();

                await _dbConnection.QueryAsync<GetAnexosReport, GetAnexosServiceReport, TotalCalculateAnexos>(
                    sql: "SP_ALL_ANEXO",
                    (anexo, service) => {
                        int countAnexos = 0;
                        if (!countAnexoDictionary.TryGetValue(anexo.idAnexo, out countAnexos))
                        {
                            totalCalculateAnexos.countAnexos += 1;
                            countAnexos += 1;
                            countAnexoDictionary.Add(anexo.idAnexo, countAnexos);
                        }

                        if (service.nameService.Equals("CEL")) 
                        {
                          totalCalculateAnexos.CELTotalSeg += service.spokenSeconds;
                          totalCalculateAnexos.CELTotalCost += service.cost;
                          totalCalculateAnexos.CELTotalCall += service.numberOfAnsweredCalls;
                        }
                        else if (service.nameService.Equals("SLM"))
                        {
                          totalCalculateAnexos.SLMTotalSeg += service.spokenSeconds;
                          totalCalculateAnexos.SLMTotalCost += service.cost;
                          totalCalculateAnexos.SLMTotalCall += service.numberOfAnsweredCalls;
                        }
                        else if (service.nameService.Equals("LDI"))
                        {
                          totalCalculateAnexos.LDITotalSeg += service.spokenSeconds;
                          totalCalculateAnexos.LDITotalCost += service.cost;
                          totalCalculateAnexos.LDITotalCall += service.numberOfAnsweredCalls;
                        }

                        totalCalculateAnexos.totalSeg += service.spokenSeconds;
                        totalCalculateAnexos.totalCost += service.cost;
                        totalCalculateAnexos.totalCallQuantity += service.numberOfAnsweredCalls;
                        return totalCalculateAnexos;
                    },
                    splitOn: "idService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return totalCalculateAnexos;
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAll] Error obteniendo tarificacion anexos: ", ex.Message);
                throw;
            }
        }
        
        public async Task<List<GetAnexoBySupervisorDto>> getAnexoBySupervisor(int idUser)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idUser", value: idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var anexDictionary = new Dictionary<int, GetAnexoBySupervisorDto>();

                var resp = await _dbConnection.QueryAsync<GetAnexoBySupervisorDto, GetServiceProvider, GetAnexoBySupervisorDto>(
                    sql: "SP_GET_ANEXO_BY_SUPERVISOR",
                    (anexos, services) => {
                        GetAnexoBySupervisorDto anexoEntry;
                        if (!anexDictionary.TryGetValue(anexos.idAnexo, out anexoEntry))
                        {
                            anexoEntry = anexos;
                            anexDictionary.Add(anexoEntry.idAnexo, anexoEntry);
                        }

                        anexoEntry.services.Add(services);

                        return anexoEntry;
                    },
                    splitOn: "nameService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.Distinct().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getAnexoBySupervisor] Error obteniendo los anexos por supervisor", ex.Message);
                throw;
            }
        }

        public async Task<List<GetDstByAnexoDto>> getDstByAnexo(int idAnexo, string initDate, string lastDate)
        {
            
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idAnexo", value: idAnexo, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_initDate", value: initDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_lastDate", value: lastDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var resp = await _dbConnection.QueryAsync<GetDstByAnexoDto>(
                    sql: "SP_GET_DST_BY_ANEXO",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                if (resp.Any())
                {
                    return resp.ToList();
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError("[getDstByAnexo] Error obteniendo  ", ex.Message);
                throw;
            }
        }

        public async Task<GetCostByAnexoUserDto> getCostByAnexoUser(GetAnexoService getAnexoService)
        {
            try
            {
                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add(name: "p_idUser", value: getAnexoService.idUser, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_idAnexo", value: getAnexoService.idAnexo, DbType.Int32, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_initDate", value: getAnexoService.initDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "p_lastDate", value: getAnexoService.lastDate, DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add(name: "outCode", direction: ParameterDirection.Output, size: DbString.DefaultLength);

                var dictionaryAnexo = new Dictionary<int, GetCostByAnexoUserDto>();

                var resp = await _dbConnection.QueryAsync<GetCostByAnexoUserDto, DataAnexoService, GetCostByAnexoUserDto>(
                    sql: "SP_GET_COST_BY_ANEXO_USER",
                    (anexos, services) => {
                        GetCostByAnexoUserDto anexoEntry;
                        if (!dictionaryAnexo.TryGetValue(anexos.idAnexo, out anexoEntry))
                        {
                            anexoEntry = anexos;
                            dictionaryAnexo.Add(anexoEntry.idAnexo, anexoEntry);
                        }

                        anexoEntry.services.Add(services);

                        return anexoEntry;
                    },
                    splitOn: "idService",
                    param: dynamicParameters,
                    commandType: CommandType.StoredProcedure
                );

                return resp.Distinct().First();
            }
            catch (Exception ex)
            {
                _logger.LogError("[getCostByAnexoUser] Error obteniendo los servicios de los anexos", ex.Message);
                throw;
            }
        }

    }
}