using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using portafolio_api.Filters;
using portafolio_api.Models.Dto.Faculty;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

namespace portafolio_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [ServiceFilter(typeof(ValidSessionFilter))]
    public class FacultyController : ControllerBase
    {
        private readonly ILogger<FacultyController> _logger;
        private readonly FacultyService _facultyService;

        public FacultyController(ILogger<FacultyController> logger, FacultyService facultyService) 
        {
            _logger = logger;
            _facultyService = facultyService;
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation("[Get] Inicio obtener facultades");
            GlobalResponse<List<GetFacultyDto>> response = new GlobalResponse<List<GetFacultyDto>>();
            try
            {
                response = await _facultyService.GetAll();
                _logger.LogInformation("[Get] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet("{idFaculty}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetById(int idFaculty)
        {
            _logger.LogInformation("[GetById] Inicio obtener facultad");
            GlobalResponse<GetFacultyDto> response = new GlobalResponse<GetFacultyDto>();
            try
            {
                response = await _facultyService.GetById(idFaculty);
                _logger.LogInformation("[GetById] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet("reportUniversity")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetReportUniversity([FromHeader(Name = "monthYear")] string monthYear)
        {
            _logger.LogInformation("[GetById] Inicio obtener reporte universidad");
            GlobalResponse<ReportAllUniversityDto> response = new GlobalResponse<ReportAllUniversityDto>();
            try
            {
                response = await _facultyService.GetReportAllUniversity(monthYear);
                _logger.LogInformation("[GetReportUniversity] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        
        [HttpGet("count")]
        [Authorize(Roles = "ADMIN,SUPERVISOR")]
        public async Task<IActionResult> GetCountAll()
        {
            _logger.LogInformation("[GetCountAll] Inicio obtener contadores");
            GlobalResponse<GetCountAll> response = new GlobalResponse<GetCountAll>();
            try
            {
                response = await _facultyService.GetAllCount();
                _logger.LogInformation("[GetCountAll] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    }
}