using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using portafolio_api.Filters;
using portafolio_api.Models.Dto.Unit;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

namespace portafolio_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [ServiceFilter(typeof(ValidSessionFilter))]
    public class UnitController : ControllerBase
    {
        private readonly ILogger<UnitController> _logger;
        private readonly UnitService _unitService;

        
        public UnitController(ILogger<UnitController> logger, UnitService unitService)
        {
            _logger = logger;
            _unitService = unitService;
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN,SUPERVISOR")]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation("[Get] Inicio obtener unidades");
            GlobalResponse<List<GetUnitDto>> response = new GlobalResponse<List<GetUnitDto>>();

            try
            {
                response = await _unitService.GetAllUnit();
                _logger.LogInformation("[Get] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetUnit(int id)
        {
            _logger.LogInformation("[GetUnit] Se inicia obtencion de unidad");
            GlobalResponse<GetUnitDto> response = new GlobalResponse<GetUnitDto>();

            try
            {
                response = await _unitService.GetId(id);
                _logger.LogInformation("[GetUnit] Controller responde correctamente");
                return Ok(response);
            }
            catch (Exception ex)
            {
                response.code = "003";
                response.message = ex.Message;
                _logger.LogError("[GetUnit] Error al obtener unidad: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Create([FromBody] CreateUpdateUnitDto createUpdateUnitDto)
        {
            _logger.LogInformation("[Create] Inicio creacion unidad");
            GlobalResponse<string> response = new GlobalResponse<string>();

            try
            {
                response = await _unitService.Create(createUpdateUnitDto);
                _logger.LogInformation("[Create] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPut("{idUnit}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Update([FromBody] CreateUpdateUnitDto createUpdateUnitDto, int idUnit)
        {
            _logger.LogInformation("[Update] Inicio actualizacion unidad");
            GlobalResponse<string> response = new GlobalResponse<string>();

            try
            {
                response = await _unitService.Update(createUpdateUnitDto, idUnit);
                _logger.LogInformation("[Update] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpDelete("{idUnit}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Delete(int idUnit)
        {
            _logger.LogInformation("[Delete] Inicio eliminacion unidad");
            GlobalResponse<List<GetUnitDto>> response = new GlobalResponse<List<GetUnitDto>>();

            try
            {
                response = await _unitService.Delete(idUnit);
                _logger.LogInformation("[Delete] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet]
        [Route("report/{initDate}/{lastDate}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetReport(string initDate, string lastDate)
        {
            _logger.LogInformation("[GetReport] Inicio obtener reporte unidades");
            GlobalResponse<object> response = new GlobalResponse<object>();

            try
            {
                response = await _unitService.GetReportUnits(initDate, lastDate);
                _logger.LogInformation("[GetReport] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

    }

}