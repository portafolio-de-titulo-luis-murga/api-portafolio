﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using portafolio_api.Filters;
using portafolio_api.Models.Dto.Provider;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

namespace portafolio_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize(Roles = "ADMIN")]
    [ServiceFilter(typeof(ValidSessionFilter))]
    public class ProviderController : ControllerBase
    {
        private readonly ILogger<ProviderController> _logger;
        private readonly ProviderService _providerService;

        public ProviderController(ILogger<ProviderController> logger, ProviderService providerService)
        {
            _logger = logger;
            _providerService = providerService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateProviderDto createProviderDto)
        {
            _logger.LogInformation("[Create] Inicio creacion de provedor");
            GlobalResponse<string> response = new GlobalResponse<string>();

            try
            {
                response = await _providerService.Create(createProviderDto);
                _logger.LogInformation("[Create] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation("[Get] Inicio obtener proveedores");
            GlobalResponse<List<GetProviderDto>> response = new GlobalResponse<List<GetProviderDto>>();

            try
            {
                response = await _providerService.GetAllProviders();
                _logger.LogInformation("[Get] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetProvider(int id)
        {
            _logger.LogInformation("[GetProvider] Inicio obtener proveedor");
            GlobalResponse<GetProviderDto> response = new GlobalResponse<GetProviderDto>();

            try
            {
                response = await _providerService.GetId(id);
                _logger.LogInformation("[GetProvider] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[GetProvider] Error al obtener proveedor: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogInformation("[Delete] Se inicia eliminacion de proveedor");
            GlobalResponse<List<GetProviderDto>> response = new GlobalResponse<List<GetProviderDto>>();
            try
            {
                response = await _providerService.Delete(id);
                _logger.LogInformation("[Delete] Controller responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[Delete] Error al eliminar proveedor: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpDelete("{id}/{newId}")]
        public async Task<IActionResult> DeleteWithFaculty(int id, int newId)
        {
            _logger.LogInformation("[DeleteWithFaculty] Se inicia eliminacion de proveedor");
            GlobalResponse<List<GetProviderDto>> response = new GlobalResponse<List<GetProviderDto>>();
            try
            {
                response = await _providerService.DeleteWithFaculty(id, newId);
                _logger.LogInformation("[DeleteWithFaculty] Controller responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[DeleteWithFaculty] Error al eliminar proveedor: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProvider([FromBody] UpdateProviderDto updateProviderDto)
        {
            _logger.LogInformation("[UpdateProvider] Se inicia actualizacion de proveedor");
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                response = await _providerService.UpdateProvider(updateProviderDto);
                _logger.LogInformation("[UpdateProvider] Controller responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[UpdateProvider] Error al actualizar proveedor: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    }
}
