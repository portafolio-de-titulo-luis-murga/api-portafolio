using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using portafolio_api.Filters;
using portafolio_api.Models.Dto.Anexos;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

namespace portafolio_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [ServiceFilter(typeof(ValidSessionFilter))]
    public class AnexoController : ControllerBase
    {
        private readonly ILogger<AnexoController> _logger;
        private readonly AnexoService _anexoService;

        public AnexoController(ILogger<AnexoController> logger, AnexoService anexoService)
        {
            _logger = logger;
            _anexoService = anexoService;
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation("[Get] Inicio obtener anexos");
            GlobalResponse<List<GetAnexoDto>> response = new GlobalResponse<List<GetAnexoDto>>();

            try
            {
                response = await _anexoService.GetAllAnexos();
                _logger.LogInformation("[Get] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        [Route("{idAnexo}")]
        public async Task<IActionResult> GetById(int idAnexo)
        {
            _logger.LogInformation("[GetById] Inicio obtene anexo");
            GlobalResponse<GetAnexoDto> response = new GlobalResponse<GetAnexoDto>();

            try
            {
                response = await _anexoService.GetById(idAnexo);
                _logger.LogInformation("[GetById] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Create([FromBody] CreateUpdateAnexoDto createUpdateAnexoDto)
        {
            _logger.LogInformation("[Create] Inicio creacion anexo");
            GlobalResponse<string> response = new GlobalResponse<string>();

            try
            {
                response = await _anexoService.Create(createUpdateAnexoDto);
                _logger.LogInformation("[Create] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPut]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Update([FromBody] CreateUpdateAnexoDto createUpdateAnexoDto)
        {
            _logger.LogInformation("[Update] Inicio actualizacion anexo");
            GlobalResponse<List<GetAnexoDto>> response = new GlobalResponse<List<GetAnexoDto>>();

            try
            {
                response = await _anexoService.Update(createUpdateAnexoDto);
                _logger.LogInformation("[Update] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        [Route("statusChange/{idAnexo}")]
        public async Task<IActionResult> UpdateStatus(int idAnexo)
        {
            _logger.LogInformation("[UpdateStatus] Inicio cambio de estado anexo");
            GlobalResponse<List<GetAnexoDto>> response = new GlobalResponse<List<GetAnexoDto>>();

            try
            {
                response = await _anexoService.UpdateStatus(idAnexo);
                _logger.LogInformation("[UpdateStatus] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        [Route("report/{initDate}/{lastDate}")]
        public async Task<IActionResult> GetReport(string initDate, string lastDate)
        {
            _logger.LogInformation("[GetReport] Inicio obtener reporte anexos");
            GlobalResponse<object> response = new GlobalResponse<object>();

            try
            {
                response = await _anexoService.GetReportAnexos(initDate, lastDate);
                _logger.LogInformation("[GetReport] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
        
        
        [HttpGet]
        [Authorize(Roles = "ADMIN,SUPERVISOR")]
        [Route("supervisor/{idUser}")]
        public async Task<IActionResult> GetAnexoBySupervisor(int idUser)
        {
            _logger.LogInformation("[GetProvider] Inicio obtener proveedor");
            GlobalResponse<List<GetAnexoBySupervisorDto>> response = new GlobalResponse<List<GetAnexoBySupervisorDto>>();

            try
            {
                response = await _anexoService.GetAnexoBySupervisor(idUser);
                _logger.LogInformation("[GetAnexoBySupervisor] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[GetProvider] Error al obtener proveedor: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }


        [HttpGet]
        [Authorize(Roles = "ADMIN,SUPERVISOR")]
        [Route("dst/{idAnexo}/{initDate}/{lastDate}")]
        public async Task<IActionResult> GetDstByAnexo(int idAnexo, string initDate, string lastDate)
        {
            _logger.LogInformation("[GetReport] Inicio obtener reporte anexos");
            GlobalResponse<List<GetDstByAnexoDto>> response = new GlobalResponse<List<GetDstByAnexoDto>>();

            try
            {
                response = await _anexoService.GetDstByAnexo(idAnexo, initDate, lastDate);
                _logger.LogInformation("[GetDstByAnexo] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN,SUPERVISOR")]
        [Route("costAnexo")]
        public async Task<IActionResult> GetCostByAnexoUser([FromBody]GetAnexoService getAnexoService)
        {
            _logger.LogInformation("[GetReport] Inicio obtener reporte anexos");
            GlobalResponse<GetCostByAnexoUserDto> response = new GlobalResponse<GetCostByAnexoUserDto>();

            try
            {
                response = await _anexoService.GetCostByAnexoUser(getAnexoService);
                _logger.LogInformation("[GetCostByAnexoUser] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    }
}