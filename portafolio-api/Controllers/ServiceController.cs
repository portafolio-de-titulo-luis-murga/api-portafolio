using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using portafolio_api.Filters;
using portafolio_api.Models.Dto.Service;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

namespace portafolio_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize(Roles = "ADMIN")]
    [ServiceFilter(typeof(ValidSessionFilter))]
    public class ServiceController : ControllerBase
    {
        private readonly ILogger<ServiceController> _logger;
        private readonly ServiceService _serviceService;

        public ServiceController(ILogger<ServiceController> logger, ServiceService serviceService)
        {
            _logger = logger;
            _serviceService = serviceService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation("[Get] Inicio obtener servicios");
            GlobalResponse<List<GetServiceDto>> response = new GlobalResponse<List<GetServiceDto>>();

            try
            {
                response = await _serviceService.GetAllService();
                _logger.LogInformation("[Get] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    }
}