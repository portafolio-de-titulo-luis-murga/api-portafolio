using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using portafolio_api.Filters;
using portafolio_api.Models.Data;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

namespace portafolio_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize(Roles = "ADMIN")]
    [ServiceFilter(typeof(ValidSessionFilter))]
    public class ReportController : ControllerBase
    {
        private readonly ILogger<AnexoController> _logger;
        private readonly ReportService _reportService;

        public ReportController(ILogger<AnexoController> logger, ReportService reportService)
        {
            _logger = logger;
            _reportService = reportService;
        }

        [HttpPost]
        [Route("generatePdf")]
        public async Task<IActionResult> GeneratePdf([FromBody] JsonElement generic)
        {
            _logger.LogInformation("[GetReport] Inicio obtener reporte anexos");
            GlobalResponse<ResponseGeneratePdf> response = new GlobalResponse<ResponseGeneratePdf>();

            try
            {
                response = await _reportService.ValidateAndGeneratePdf(generic);
                _logger.LogInformation("[GetReport] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    }
}