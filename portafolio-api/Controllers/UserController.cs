using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using portafolio_api.Filters;
using portafolio_api.Models;
using portafolio_api.Models.Dto.User;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

namespace portafolio_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [ServiceFilter(typeof(ValidSessionFilter))]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly UserService _userService;

        public UserController(ILogger<UserController> logger, UserService userService) 
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Create([FromBody] CreateUserDto createUserDto)
        {
            _logger.LogInformation("[Create] Inicio creacion de usuario");
            GlobalResponse<string> response = new GlobalResponse<string>();

            try
            {
                response = await _userService.Create(createUserDto);
                _logger.LogInformation("[Create] Response correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation("[Get] Inicio obtener usuarios");
            GlobalResponse<List<GetUserDto>> response = new GlobalResponse<List<GetUserDto>>();

            try
            {
                response = await _userService.GetAll();
                _logger.LogInformation("[Get] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet("{idUser}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetById(int idUser)
        {
            _logger.LogInformation("[GetById] Inicio obtene usuario");
            GlobalResponse<GetUserDto> response = new GlobalResponse<GetUserDto>();

            try
            {
                response = await _userService.GetById(idUser);
                _logger.LogInformation("[GetById] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    
        [HttpPost("report/{idUser}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetReportByUser(int idUser, [FromBody] DateFilterDto dateFilterDto)
        {
            _logger.LogInformation("[GetReportByUser] Inicio obtiene data reporte responsable");
            GlobalResponse<UserDataReport> response = new GlobalResponse<UserDataReport>();

            try
            {
                response = await _userService.GetPdfReport(idUser, dateFilterDto.dateFilter);
                _logger.LogInformation("[GetReportByUser] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPost("report/excel/{idUser}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetReportExcelByUser(int idUser, [FromBody] DateFilterDto dateFilterDto)
        {
            _logger.LogInformation("[GetReportExcelByUser] Inicio obtiene data reporte responsable");
            GlobalResponse<List<UserReportAnexoExcel>> response = new GlobalResponse<List<UserReportAnexoExcel>>();

            try
            {
                response = await _userService.GetExcelReport(idUser, dateFilterDto.dateFilter);
                _logger.LogInformation("[GetReportExcelByUser] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet("responsable")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> GetResponsables()
        {
            _logger.LogInformation("[Get] Inicio obtener unidades");
            GlobalResponse<List<GetResponsableDto>> response = new GlobalResponse<List<GetResponsableDto>>();

            try
            {
                response = await _userService.GetAllResponsables();
                _logger.LogInformation("[GetResponsables] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet("resetPassword/{idUser}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> ResetPW(int idUser)
        {
            _logger.LogInformation("[ResetPW] Inicio reestablecer contraseña");
            GlobalResponse<string> response = new GlobalResponse<string>();

            try
            {
                response = await _userService.ResetPW(idUser);
                _logger.LogInformation("[ResetPW] Responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpDelete("{idUser}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> DeleteUser(int idUser)
        {
            _logger.LogInformation("[Delete] Se inicia eliminacion de proveedor");
            GlobalResponse<List<GetUserDto>> response = new GlobalResponse<List<GetUserDto>>();
            try
            {
                response = await _userService.DeleteUser(idUser);
                _logger.LogInformation("[DeleteUser] Controller responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[DeleteUser] Error al eliminar proveedor: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpDelete("{idUser}/{idNewUser}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> DeleteUserWithUnit(int idUser, int idNewUser)
        {
            _logger.LogInformation("[DeleteUserWithUnit] Se inicia eliminacion de usuario");
            GlobalResponse<List<GetUserDto>> response = new GlobalResponse<List<GetUserDto>>();
            try
            {
                response = await _userService.DeleteUserWithUnit(idUser, idNewUser);
                _logger.LogInformation("[DeleteUserWithUnit] Controller responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[DeleteUserWithUnit] Error al eliminar usuario: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPut("{idUser}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUser updateUser, int idUser)
        {
            _logger.LogInformation("[UpdateUser] Se inicia actualizacion de usuario");
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                response = await _userService.UpdateUser(updateUser, idUser);
                _logger.LogInformation("[UpdateUser] Controller responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[UpdateUser] Error al actualizar usuario: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPut("updatePwd")]
        [Authorize(Roles = "ADMIN,SUPERVISOR")]
        public async Task<IActionResult> UpdatePwd([FromBody] UpdatePwdDto updatePwdDto)
        {
            _logger.LogInformation("[UpdatePwd] Se inicia actualizacion de contraseña");
            GlobalResponse<string> response = new GlobalResponse<string>();
            try
            {
                response = await _userService.UpdatePwd(updatePwdDto);
                _logger.LogInformation("[UpdatePwd] Controller responde correctamente");
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                _logger.LogError("[UpdatePwd] Error al actualizar contraseña: ", ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    
    }
}