﻿using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Asn1.Ocsp;
using portafolio_api.Models.Dto.Auth;
using portafolio_api.Models.Exceptions;
using portafolio_api.Models.Response;
using portafolio_api.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace portafolio_api.Controllers
{
    // [Consumes("aplication/json")]
    // [Produces("aplication/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        //log es para los errores y seguimiento de flujo, saber como respondió
        private readonly ILogger<AuthController> _logger;
        private readonly AuthService _authService;

        //constructor del controlador
        public AuthController(ILogger<AuthController> logger, AuthService authService) 
        { 
            _logger = logger; 
            _authService = authService;
        }

        [HttpPost]
        [Route("login")]
        //metodo
        public async Task<IActionResult> Login([FromBody] LoginDto loginDto) 
        {
            GlobalResponse<LoginResponseDto> response = new GlobalResponse<LoginResponseDto>();
            try
            {
                response = await _authService.Login(loginDto);
                return Ok(response);
            }
            catch (CustomErrorsExceptions cee)
            {
                response.code = cee._code;
                response.message = cee.Message;
                return StatusCode(cee._httpCode, response);
            }
            catch (Exception ex)
            {
                response.code = "999";
                response.message = ex.Message;
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
    }
}
